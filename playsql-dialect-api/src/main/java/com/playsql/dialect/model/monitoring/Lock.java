package com.playsql.dialect.model.monitoring;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * Represents the details of an SQL lock, as to be displayed in the front-end.
 */
public class Lock {
    private String relation;
    private String mode;
    private String locktype;
    private String page;
    private String tuple;
    private String transactionid;
    private String pid;
    private String granted;
    private String fastpath;

    //<editor-fold desc="Wires">
    public Lock(String relation, String mode, String locktype, String page, String tuple, String transactionid, String pid, String granted, String fastpath) {
        this.relation = relation;
        this.mode = mode;
        this.locktype = locktype;
        this.page = page;
        this.tuple = tuple;
        this.transactionid = transactionid;
        this.pid = pid;
        this.granted = granted;
        this.fastpath = fastpath;
    }

    public Lock() {
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getLocktype() {
        return locktype;
    }

    public void setLocktype(String locktype) {
        this.locktype = locktype;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTuple() {
        return tuple;
    }

    public void setTuple(String tuple) {
        this.tuple = tuple;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getGranted() {
        return granted;
    }

    public void setGranted(String granted) {
        this.granted = granted;
    }

    public String getFastpath() {
        return fastpath;
    }

    public void setFastpath(String fastpath) {
        this.fastpath = fastpath;
    }
//</editor-fold>
}
