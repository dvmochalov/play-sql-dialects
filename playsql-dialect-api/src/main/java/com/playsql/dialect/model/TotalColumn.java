package com.playsql.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the details of a total. It's called column because it is true on the SQL side, but it's usually
 * presented at the bottom of a table and spans over multiple columns.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TotalColumn {
    private String name;
    private String label;
    private String calculation;
    private int colspan;

    public TotalColumn() {
        // JAXB Noarg constructor
    }

    public TotalColumn(String name, String label, String calculation, int colspan) {
        this.name = name;
        this.label = label;
        this.calculation = calculation;
        this.colspan = colspan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCalculation() {
        return calculation;
    }

    public void setCalculation(String calculation) {
        this.calculation = calculation;
    }

    public int getColspan() {
        return colspan;
    }

    public void setColspan(int colspan) {
        this.colspan = colspan;
    }

    public TotalColumn copy(TotalColumn result) {
        if (result == null) result = new TotalColumn();
        result.name = name;
        result.label = label;
        result.calculation = calculation;
        result.colspan = colspan;
        return result;
    }
}
