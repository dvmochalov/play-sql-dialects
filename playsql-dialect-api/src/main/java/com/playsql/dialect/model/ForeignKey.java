package com.playsql.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Objects;

import static com.playsql.spi.utils.PlaySqlUtils.equal;


/**
 * Properties of a column which is a private key to another table.
 *
 * The origin column isn't detailed here.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ForeignKey implements Cloneable {

    public enum Action {
        NO_ACTION("a", "NO ACTION"),
        RESTRICT("r", "RESTRICT"),
        CASCADE("c", "CASCADE"),
        SET_NULL("n", "SET NULL"),
        SET_DEFAULT("d", "SET DEFAULT");

        private final String sqlFlag;
        private final String sqlExpression;

        Action(String sqlFlag, String sqlExpression) {
            this.sqlFlag = sqlFlag;
            this.sqlExpression = sqlExpression;
        }

        public String getSqlExpression() {
            return sqlExpression;
        }

        public static Action parse(String fkAction) {
            if (fkAction == null) return null;
            for (Action value : values())
                if (equal(value.sqlFlag, fkAction))
                    return value;
            throw new IllegalArgumentException("Foreign key action is unparseable: " + fkAction);
        }
    }

    // == All properties hard-wired in SQL ==
    private String targetTable;
    private Action onDelete;
    private Action onUpdate; // If null, equals onDelete
    /** The name of the constraint in SQL */
    @XmlTransient
    private String constraintName;
    /** Name of the target of the SQL FK constraint. It's the primary key in 99% cases. */
    private String targetPK;

    /** == All properties stored in the metadata – See ColumnComment == */
    private String displayField;
    @XmlTransient
    private String displayFieldSQL;
    private String searchField;
    /** DDL of the display field - We need it in case the format should be displayed in a certain way. */
    private DDLColumn targetDisplayFieldDDL;
    /**
     * Default true: FKs with an SQL constraint are reflected at the SQL level, so no other program can enter
     * invalid values.
     *
     * A foreign key has at least one of withSQLConstraint or withUI.
     */
    private boolean withSQLConstraint;

    /**
     * Default true: FKs with UI have a displayColumn and a searchColumn. It is independent from
     * isTechnicallyEnforced. If true or default, Play SQL will try to determine the most suitable
     * columns for search/display. If false, Play SQL will only display the original value.
     */
    private boolean withUI;

    public ForeignKey() {
        // JAXB
    }

    public ForeignKey(String constraintName, String targetTable, String targetPK, String displayField, String searchField, Action onDelete, boolean withSQLConstraint, boolean withUI) {
        this.constraintName = constraintName;
        this.targetTable = targetTable;
        this.targetPK = targetPK;
        this.displayField = displayField;
        this.searchField = searchField;
        this.onDelete = onDelete;
        this.onUpdate = null;
        this.withSQLConstraint = withSQLConstraint;
        this.withUI = withUI;

    }

    public static ForeignKey copy(ForeignKey fk) {
        if (fk == null) return null;
        try {
            return (ForeignKey) fk.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public String getTargetTable() {
        return targetTable;
    }

    public Action getOnDelete() {
        return onDelete;
    }

    public Action getOnUpdate() {
        return onUpdate;
    }

    public String getDisplayField() {
        return displayField;
    }

    public String getSearchField() {
        return searchField;
    }

    public ForeignKey setTargetTable(String targetTable) {
        this.targetTable = targetTable;
        return this;
    }

    public ForeignKey setOnDelete(Action onDelete) {
        this.onDelete = onDelete;
        return this;
    }

    public ForeignKey setOnUpdate(Action onUpdate) {
        this.onUpdate = onUpdate;
        return this;
    }

    public ForeignKey setDisplayField(String displayField) {
        this.displayField = displayField;
        return this;
    }

    public ForeignKey setSearchField(String searchField) {
        this.searchField = searchField;
        return this;
    }

    public void setConstraintName(String constraintName) {
        this.constraintName = constraintName;
    }

    public String getTargetPK() {
        return targetPK;
    }

    public void setTargetPK(String targetPK) {
        this.targetPK = targetPK;
    }

    public DDLColumn getTargetDisplayFieldDDL() {
        return targetDisplayFieldDDL;
    }

    public void setTargetDisplayFieldDDL(DDLColumn targetDisplayFieldDDL) {
        this.targetDisplayFieldDDL = targetDisplayFieldDDL;
    }

    public String getDisplayFieldSQL() {
        return displayFieldSQL;
    }

    public void setDisplayFieldSQL(String displayFieldSQL) {
        this.displayFieldSQL = displayFieldSQL;
    }

    public String getConstraintName() {
        return constraintName;
    }

    public boolean isWithSQLConstraint() {
        return withSQLConstraint;
    }

    public void setWithSQLConstraint(boolean withSQLConstraint) {
        this.withSQLConstraint = withSQLConstraint;
    }

    public boolean isWithUI() {
        return withUI;
    }

    public void setWithUI(boolean withUI) {
        this.withUI = withUI;
    }

    @Override
    public String toString() {
        return String.format("FK{%s -> %s.%s, display=%s, search=%s, %s/%s%s}",
            constraintName,
            targetTable,
            targetPK,
            displayField,
            searchField,
            Objects.toString(onDelete),
            Objects.toString(onUpdate),
            targetDisplayFieldDDL != null ? " ddl" : "");
    }
}
