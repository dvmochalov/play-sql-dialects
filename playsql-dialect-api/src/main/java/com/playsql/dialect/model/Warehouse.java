package com.playsql.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import static com.playsql.spi.utils.PlaySqlUtils.equal;


public class Warehouse {
    private final String schemaName;
    /** The key MUST be the context.asString AND the schemaName MUST be a bijection of the key */
    private String key;
    private String label;
    private String self;
    private String avatarUrl;
    private String spaceLabel;

    public Warehouse(){
        //gson creation
        key = "";
        label = "";
        schemaName = "";
    }

    public Warehouse(String schemaName, String key, String label) {
        this.schemaName = schemaName;
        this.key = key;
        this.label = label;
    }

    public String getKey() {
        return key;
    }

    public String getLabel() {
        return label;
    }

    public String getSpace() {
        // We can't use the 'Context' class because it's defined at a higher level
        if (key != null && key.startsWith("space:")) {
            return key.substring("space:".length());
        }
        return null;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getSelf() {
        return self;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getSpaceLabel() {
        return spaceLabel;
    }

    public void setSpaceLabel(String spaceLabel) {
        this.spaceLabel = spaceLabel;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return String.format("Warehouse{%s \"%s\"}",
                key,
                label);
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /** Finds the warehouse by key
     * @param schemaName the schema name
     **/
    public static Warehouse findBySchemaName(List<Warehouse> warehouseList, String schemaName) {
        if (warehouseList != null)
            for (Warehouse warehouse : warehouseList)
                if (equal(warehouse.getSchemaName(), schemaName))
                    return warehouse;
        return null;
    }

    public static Warehouse findByKey(List<Warehouse> warehouses, String warehouseKey) {
        if (warehouses != null)
            for (Warehouse warehouse : warehouses)
                if (equal(warehouse.getKey(), warehouseKey))
                    return warehouse;
        return null;
    }
}
