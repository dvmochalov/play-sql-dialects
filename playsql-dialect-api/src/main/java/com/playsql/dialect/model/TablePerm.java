package com.playsql.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Set;

import static com.playsql.spi.utils.PlaySqlUtils.equal;


/**
 * Permissions for the table.
 * <p/>
 * Some permissions are atomic, others are groups of atomic permissions.
 * If you update them, please update {@code ss.js} in the front-end.
 * <p/>
 * This class is clearly used for front-end features. But because the {@link DDLTable} bean is
 * used all the way through the front-end and serialized in ajax calls, we need to make a
 * small concession in the model of separation of concerns.
 */
public enum TablePerm {


    /* === Atomic permissions == */

    VIEW,

    ADD_ROWS,
    EDIT_CELLS,
    REMOVE_ROWS,
    EDIT_ROWS_METADATA,
    EDIT_TOTALS,
    ADD_COLS,
    /** Edit front-end properties: Label, Renderer */
    EDIT_COLS_FE,
    /** Edit back-end properties: Name, Type */
    EDIT_COLS_BE,
    REMOVE_COLS,

    REMOVE_TABLE,




    /* == Permission groups for Queries == */

    // If you update the groups, please update parsePerms() in ss.js
    /** Can display a query */
    Q_VIEW(VIEW),
    /** Can edit cells which have been explicitly marked as editable, known as 'joints' */
    Q_VIEW_EDIT(Q_VIEW),
    /** Can fiddle with a query, modify it an re-run it. Can modify the totals. */
    Q_EDIT(Q_VIEW_EDIT, EDIT_TOTALS, EDIT_COLS_FE),
    /** Can create, save and delete a query and the fields of joint tables. */
    Q_SAVE(Q_EDIT),
    /** Can edit the right-hand cells of a joint table, but not its totals */
    JT_EDIT(Q_VIEW_EDIT, EDIT_COLS_FE),

    /* == Permission groups for Spreadsheets == */
    //
    // If you update the groups, please update parsePerms() in ss.js
    EDIT(VIEW, ADD_ROWS, EDIT_CELLS, REMOVE_ROWS, EDIT_ROWS_METADATA, Q_VIEW_EDIT),
    ADMIN_COLS(EDIT, EDIT_TOTALS, ADD_COLS, EDIT_COLS_FE, EDIT_COLS_BE, REMOVE_COLS),
    CONTEXT_ADMIN(ADMIN_COLS, REMOVE_TABLE);
;





    /* === Methods === */

    TablePerm(TablePerm... members) {
        this.members = members;
    }

    private TablePerm[] members;

    public boolean includes(TablePerm candidate) {
        if (this == candidate) return true;
        if (members != null)
            for (TablePerm member : members)
                if (member.includes(candidate))
                    return true;
        return false;
    }
    public List<TablePerm> children() {
        List<TablePerm> result = Lists.newArrayList();
        for (TablePerm child : members) {
            result.add(child);
            result.addAll(child.children());
        }
        return result;
    }

    public static List<TablePerm> expand(List<TablePerm> perms) {
        Set<TablePerm> result = Sets.newHashSet();
        for (TablePerm perm: perms) {
            result.add(perm);
            result.addAll(perm.children());
        }
        return Lists.newArrayList(result);
    }

    /**
     * Returns the intersection of the original and provided permissions. It's symmetric for 'original' and 'permissions'.
     * @param original the original list of permissions.
     * @param permissions
     * @return
     */
    public static List<TablePerm> intersect(List<TablePerm> original, TablePerm... permissions) {
        List<TablePerm> commonItems = expand(Lists.newArrayList(permissions));
        List<TablePerm> originalItems = expand(original);
        originalItems.retainAll(commonItems);
        return commonItems;
    }

    /**
     * Translate the permlevel from the macro's parameter (which is more a human-readable string)
     * @return never null. ADMIN_COLS by default.
     */
    public static TablePerm translateFromMacro(String permlevel) {
        permlevel = StringUtils.upperCase(permlevel);
        if (equal(permlevel, "VIEW")) {
            return TablePerm.VIEW;
        } else if (equal(permlevel, "EDIT")) {
            return TablePerm.EDIT;
        } else if (equal(permlevel, "ADMIN")) {
            return TablePerm.ADMIN_COLS;
        }
        return TablePerm.ADMIN_COLS;
    }
}
