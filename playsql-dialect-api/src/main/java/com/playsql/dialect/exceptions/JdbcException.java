package com.playsql.dialect.exceptions;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.api.Jdbc;

/**
 * Exception thrown by {@link Jdbc}
 */
public class JdbcException extends RuntimeException
{
    final private String sql;
    public JdbcException()
    {
        super();
        sql = null;
    }

    public JdbcException(String message, String sql, Throwable cause)
    {
        super(message, cause);
        this.sql = sql;
    }

    public String getSql() {
        return sql;
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();
        if (getCause() != null && getCause().getMessage() != null) {
            message = (message != null ? message + "\n" : "") + getCause().getMessage() + "\n" + (sql != null ? "Query:" + sql : "");
        } else {
            message = (message != null ? message + " -\n": "") + (sql != null ? "Query:" + sql : "");
        }
        return message;
    }

    public String getMessageOnly() {
        String message = super.getMessage();
        if (getCause() != null) {
            if (getCause().getMessage() != null) {
                message = (message != null ? message + "\n" : "") + getCause().getMessage();
            } else {
                message = (message != null ? message + "\n" : "") + getCause().toString();
            }
        }
        return message;
    }
}