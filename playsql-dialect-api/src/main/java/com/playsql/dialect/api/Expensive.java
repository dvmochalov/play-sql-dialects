package com.playsql.dialect.api;


import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({
  ElementType.METHOD,
  ElementType.CONSTRUCTOR,
  ElementType.TYPE
  })
@Inherited
public @interface Expensive {
    String value() default "This method is unnecessarily expensive";
}
