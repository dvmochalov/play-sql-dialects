package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Function;
import com.playsql.dialect.model.DDLColumn;
import com.playsql.spi.models.Tuple;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.Types;
import java.util.List;

public interface CapabilityDataManagement {
    <T> List<T> readData(String tablename, String selectFields, String whereClause,
                         String order, Long limit, Long offset,
                         String pkvalue,
                         ResultSetExtractor resultSetReader);

    /**
     * Read 1 row
     * @param tablename the table name. Not escaped.
     * @param field the field to read. Not escaped.
     * @param pkvalue the value of the ID column
     * @param resultType the class of the result type
     * @return the value, or null
     */
    <T> T readRecord(String tablename, String field, String pkColumn, int pkvalue, Class<T> resultType);

    DDLColumn.DataType fromSqlType(Integer sqlType);

    /** Return the javax.sql.Types value for the argument
     * @return an integer value which means something against {@link Types} */
    int toSqlType(DDLColumn.DataType type);

    Integer writeData(String tablename, List<String> colNamesList, List<Object> arguments, String pkvalue);

    /**
     * @return the id of the newly inserted row
     */
    String insertData(String tablename, List<String> colNamesList, List<Object> arguments);

    int removeData(String tablename, String pkvalue);

    void executeRowMetadataUpgradeTask(boolean includeNulls, String tableName, Function<Tuple<String, String>, String> transformation);
}
