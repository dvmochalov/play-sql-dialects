package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.exceptions.JdbcException;
import com.playsql.dialect.model.DDLColumn;
import com.playsql.dialect.model.DDLTable;
import com.playsql.dialect.model.ForeignKey;
import com.playsql.spi.models.Tuple;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface CapabilityTableManagement {
    /**
     * Builds the DDL for the table.
     *
     * @param tablename the tablename
     * @param includeDeletedColumns whether the deleted columns should be returned
     * @return the ddl
     * @throws JdbcException if table doesn't exist
     */
    DDLTable readTableDefinition(String tablename, boolean includeDeletedColumns);

    void createTable(DDLTable table);

    void renameTable(String original, String newName);

    /** Check if {@code table} exists (case insensitive) */
    void duplicateTable(String copyOf, DDLTable newTable);

    boolean hasTable(String table);

    void dropTable(DDLTable table, boolean ifExists, DDLDialect.DropBehaviour behaviour);

    void updateTableMetadata(DDLTable result);

    void addColumn(DDLTable table, DDLColumn column, DDLColumn beforeColumn);

    void dropColumn(DDLTable table, DDLColumn column, DDLDialect.DropBehaviour behaviour);

    void renameColumn(DDLTable table, DDLColumn originalColumn, DDLColumn newColumn);

    void changeColumnType(DDLTable definition, DDLColumn originalColumn, DDLColumn newColumn);

    void moveColumn(DDLTable definition, String columnname, String before);

    void changeColumnLabelFormulaWidthFKAndRenderer(DDLTable definition, DDLColumn originalColumn, DDLColumn column);
    // void changeColumnRenderer(DDLTable definition, DDLColumn column);

    void changeColumnFormats(DDLTable definition, DDLColumn originalColumn, DDLColumn column);

    /*** Foreign keys ***/

    void fkAdd(DDLTable table, DDLColumn column, ForeignKey fk, String FK_RENDERER_KEY);

    // void updateColumn(DDLTable table,DDLTable targetTable , DDLColumn column, DDLColumn targetColumn);

    // void setSpreadsheetList(String contextString, String list);

    // void updateCommentOnColumnWithFk(String tableName, String columnName, String newColumnName, boolean flag);

    void updateBinaryData(String tableName, String columnName, int id, byte[] data);

    void copyBinaryData(
        String fromTable, String fromColumn, int fromId,
        String toTable, String toColumn, int toId);

    void getBinaryData(String tableName, String columnName, int id, Consumer<InputStream> consumer);




    /**
     * Remove a foreign key.
     * @param logicalFk If there is an FK to keep without the SQL constraint, please provide it in the comments.
     */
    void fkRemove(DDLTable table, DDLColumn column, ForeignKey logicalFk);

    void fkSaveLogicalDetails(DDLTable table, DDLColumn column, ForeignKey fk);

    void fkInsertMissingValues(String originTable, DDLColumn originColumn,
                               String targetTable, DDLColumn targetColumn,
                               DDLColumn otherTextColumn);

    void fkReplaceValueWithIDs(String originTable, DDLColumn originDisplayColumn, DDLColumn originIdColumn,
                               String targetTable, DDLColumn targetDisplayColumn, DDLColumn targetIdColumn);

    void fkAddFkAndConvertValues(DDLTable originTable, DDLColumn originColumn,
                                 String targetTable, DDLColumn targetIdColumnDDL, DDLColumn targetDisplayColumnDDL,
                                 ForeignKey fk, String FK_RENDERER_KEY);

    List<Tuple<DDLTable, DDLColumn>> getListLinkedTables(String tableName);
}
