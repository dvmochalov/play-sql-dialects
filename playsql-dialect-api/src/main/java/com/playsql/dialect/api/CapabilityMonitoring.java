package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.model.monitoring.Cursor;
import com.playsql.dialect.model.monitoring.Lock;
import com.playsql.dialect.model.monitoring.RunningQuery;

import java.util.List;
import java.util.Map;

public interface CapabilityMonitoring {
    List<RunningQuery> getMonitoringActivity(Long id);

    List<Lock> getMonitoringLocks();

    List<Cursor> getMonitoringCursors();

    List<Map<String, String>> getMonitoringBlockingActivity();

    String monitoringTerminateBackend(Long id);
}
