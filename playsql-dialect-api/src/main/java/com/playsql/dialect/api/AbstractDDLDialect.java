package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.playsql.dialect.model.xml.DialectConfig;

import java.util.List;

import static com.playsql.spi.utils.PlaySqlUtils.equal;


/**
 * This class provides a few helpers for dialects. Please use it in conjunction with the
 * annotation {@code @}{@link DialectConfig}.
 */
public abstract class AbstractDDLDialect implements DDLDialect {

    /* List of dialect keys, with at least one element initialized in the constructor,
     * which is the key of this dialect. The next
     * elements are the keys of the inherited dialects.
     */
    final private String key;
    /** Name of the files containing the query templates */
    final private List<String> files = Lists.newArrayList();
    final private String i18nName;
    final private CaseSensibility defaultCase;
    final private boolean capabilityReadWrite;
    final private boolean capabilityAutocomplete;
    final private boolean capabilityMonitoring;
    final private String titleText;
    final private String titleLink;
    final private String driverName;
    final private String exampleUrl;
    final private String minimumDriverString;
    final private String minimumUrlString;
    protected Jdbc jdbc;

    public AbstractDDLDialect() {
        DialectInfo annotation = this.getClass().getAnnotation(DialectInfo.class);
        if (annotation == null) throw new IllegalArgumentException("Missing annotation @DialectInfo on " + this.getClass().getCanonicalName());
        i18nName = annotation.i18n();
        titleText = annotation.titleText();
        titleLink = annotation.titleLink();
        defaultCase = annotation.defaultCase();
        capabilityReadWrite = annotation.readWrite();
        capabilityAutocomplete = annotation.autocomplete();
        capabilityMonitoring = annotation.monitoring();
        driverName = annotation.driverName();
        exampleUrl = annotation.exampleUrl();
        minimumDriverString = annotation.minimumDriverString();
        minimumUrlString = annotation.minimumUrlString();

        annotation = this.getClass().getAnnotation(DialectInfo.class);
        key = annotation.key();
    }

    public Object templates() {
        return null;
    }

    @Override
    final public String getDialectI18nName() {
        return i18nName;
    }

    public String getDialectKey() {
        return key;
    }

    protected Jdbc jdbc() {
        if (jdbc != null) {
            return jdbc;
        }
        throw new IllegalStateException("The dialect needs to be injected with the connection handle before it's used");
    }

    @Override
    final public CaseSensibility getDefaultCase() {
        return defaultCase;
    }

    @Override
    final public boolean isCapabilityReadwrite() {
        return capabilityReadWrite;
    }

    @Override
    final public boolean isCapabilityAutocomplete() {
        return capabilityAutocomplete;
    }

    @Override
    final public boolean isCapabilityMonitoring() {
        return capabilityMonitoring;
    }

    final public String getMinimumUrlString() {
        return minimumUrlString;
    }

    final public String getMinimumDriverString() {
        return minimumDriverString;
    }

    final public String getExampleUrl() {
        return exampleUrl;
    }

    final public String getDriverName() {
        return driverName;
    }

    final public String getTitleLink() {
        return titleLink;
    }

    final public String getTitleText() {
        return titleText;
    }

    /** /!\ Children must reimplement this method. */
    @Override
    public boolean is(String dialectKey) {
        return equal(dialectKey, key);
    }

    public DDLDialect clone(Jdbc jdbc) {
        try {
            // Shallow copy, since we're just reusing the (potentially singleton) services injected in the final fields.
            AbstractDDLDialect clone = (AbstractDDLDialect) clone();
            clone.jdbc = jdbc;
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Jdbc getJdbc() {
        return jdbc;
    }
}
