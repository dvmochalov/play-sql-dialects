package com.playsql.jdbc.dialect.templates

class TemplatesForMySQL : TemplatesForGeneric() {

    override fun hasSchema(): String {
        val query = """
            SELECT COUNT(*)
            FROM information_schema.schemata
            WHERE schema_name = ?
        """
        return query
    }


    override fun hasTable(): String {
        val query = """
            select COUNT(*)
            from  INFORMATION_SCHEMA.TABLES
            where LOWER(TABLE_NAME) = LOWER(?)
            and TABLE_SCHEMA = SCHEMA()
        """
        return query
    }


    override fun getCommentOnTable(): String {
        val query = """
            SELECT
            table_name TABLENAME,
            table_comment COM
            FROM INFORMATION_SCHEMA.TABLES
            WHERE table_schema = SCHEMA()
            AND table_name = ?
        """
        return query
    }


    override fun setDefaultSchemas(schemaName: String): String {
        val query = """
            USE `$schemaName`
        """
        return query
    }


    override fun currentUserAndSchema(): String {
        val query = """
            SELECT user(), schema()
        """
        return query
    }

    override fun listTablesWithoutSchema(startingWith: Boolean, withIntegerColumn: Boolean, withCurrentSchema: Boolean): String {
        val query = """
            SELECT table_schema, table_name
            FROM information_schema.tables t
            WHERE
            ${if (withCurrentSchema) "upper(table_schema) = upper(SCHEMA())" else " 1=1 "}
            ${if (startingWith) "AND upper(table_name) LIKE ?" else ""}
        """
        return query
    }


    override fun count101(tableName: String,
                          whereClause: String?): String {
        val query = """
            SELECT COUNT(*) FROM (
            SELECT 1 FROM $tableName
            ${where(whereClause)}
            LIMIT 101
            ) SUBTABLE
        """
        return query
    }


    override fun readData(fieldlist: String,
                          limit: Long?,
                          offset: Long?,
                          order: String?,
                          tablename: String,
                          whereClause: String?): String {
        val query = """
            SELECT $fieldlist
            FROM $tablename TARGET_TABLE
            ${where(whereClause)}
            ${order(order)}
            ${limit(limit)}
            ${offset(offset)}
        """
        return query
    }


}