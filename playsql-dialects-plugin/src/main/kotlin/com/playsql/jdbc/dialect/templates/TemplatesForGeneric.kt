/*
  #%L
  Play SQL Dialects
  %%
  Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
*/

package com.playsql.jdbc.dialect.templates;

open class TemplatesForGeneric {

	/*
    This helps to build complex sql queries. Examples: 
    "SELECT * FROM dual ${helper("WHERE", "some condition")}" => SELECT * FROM dual WHERE some condition
    "SELECT * FROM dual ${helper("WHERE", null)}" => SELECT * FROM dual
    "SELECT * FROM dual ${helper("WHERE", null, "WHERE TRUE")}" +> SELECT * FROM dual WHERE TRUE
    */
	fun helper(keyword: String, value: Any?, default: String = ""): String {
		if (value == null) return default
		else return "$keyword $value"
	}
	fun where(value: Any?, default: String = "") = helper("WHERE", value, default)
	fun order(value: Any?, default: String = "") = helper("ORDER BY", value, default)
	fun limit(value: Any?, default: String = "") = helper("LIMIT", value, default)
	fun offset(value: Any?, default: String = "") = helper("OFFSET", value, default)
	fun and(value: Any?, default: String = "") = helper("AND", value, default)


	open fun currentSchema(): String {
		val query = """
        select CURRENT_SCHEMA from (VALUES(1))
		"""
		return query
	}

	open fun hasSchema(): String {
		val query = """
        SELECT COUNT(*) FROM dba_users WHERE username = ?
		"""
		return query
	}


	open fun createSchema(schemaName: String): String {
		val query = """
        CREATE SCHEMA "$schemaName"
		"""
		return query
	}


	open fun dropSchema(schemaName: String): String {
		val query = """
        DROP SCHEMA "$schemaName" CASCADE
		"""
		return query
	}


	open fun listSchemas(name: String?): String {
		val query = """
        SELECT n.nspname schema_name,
          pg_catalog.pg_get_userbyid(n.nspowner) AS owner,
          pg_catalog.array_to_string(n.nspacl, E'\n') AS privileges,
          pg_catalog.obj_description(n.oid, 'pg_namespace') AS desc
        FROM pg_catalog.pg_namespace n
        WHERE LOWER(n.nspname) NOT IN ('information_schema', 'public')
        and LOWER(n.nspname) NOT LIKE 'pg_%'
        ${if (name != null) "and n.nspname = ?" else ""}
        ORDER BY 1
		"""
		return query
	}


	open fun updateSchemaComment(schema: String, comment: String): String {
		val query = """
        COMMENT ON SCHEMA $schema IS $comment
		"""
		return query
	}


	open fun getDefaultSchemas(): String {
		val query = """
        SHOW search_path
		"""
		return query
	}


	open fun setDefaultSchemas(schemaName: String): String {
		val query = """
        SET search_path TO "$schemaName"
		"""
		return query
	}


	open fun listTablesWithoutSchema(startingWith: Boolean, withIntegerColumn: Boolean, withCurrentSchema: Boolean): String {
		throw RuntimeException("listTablesWithoutSchema isn't implemented in the Generic dialect")
	}

	open fun listTablesWithSchema(startingWith: Boolean, withIntegerColumn: Boolean): String {
		val query = """
        select    T.TABLE_SCHEMA, T.TABLE_NAME -- , C.COMMENT COM
        from      INFORMATION_SCHEMA.TABLES T

        left join INFORMATION_SCHEMA.SYSTEM_COMMENTS C
                on T.TABLE_CATALOG = C.OBJECT_CATALOG
                and T.TABLE_SCHEMA = C.OBJECT_SCHEMA
                and T.TABLE_NAME = C.OBJECT_NAME
                and C.OBJECT_TYPE='TABLE'

        where     UPPER(TABLE_SCHEMA)=UPPER(?)
        ${if (startingWith) "and UPPER(TABLE_NAME) LIKE (?)" else ""}      
		"""
		return query
	}


	open fun listTablesAndComments(): String {
		val query = """
        select T.TABLE_NAME tablename, C.COMMENT com
        from INFORMATION_SCHEMA.TABLES T
        left join INFORMATION_SCHEMA.SYSTEM_COMMENTS C
            on T.TABLE_CATALOG = C.OBJECT_CATALOG
            and T.TABLE_SCHEMA = C.OBJECT_SCHEMA
            and T.TABLE_NAME = C.OBJECT_NAME
            and C.OBJECT_TYPE='TABLE'
        where
            TABLE_SCHEMA=CURRENT_SCHEMA
		"""
		return query
	}


	open fun currentUserAndSchema(): String {
		val query = """
      select
      sys_context( 'USERENV', 'CURRENT_SCHEMA' ) CURRENT_SCHEMA,
      sys_context( 'USERENV', 'CURRENT_USER') CURRENT_USER
      from dual
		"""
		return query
	}


	open fun count101(tableName: String, whereClause: String?): String {
		val query = """
      SELECT COUNT(*) FROM (
        SELECT 1 FROM $tableName
        WHERE ROWNUM <= 100
        ${and(whereClause)}
      ) SUBTABLE
		"""
		return query
	}
	open fun getBinaryData(tableName: String, columnName: String, id: Int): String {
		val query = """
      	SELECT $columnName FROM $tableName
		WHERE "ID" =$id
		"""
		return query
	}

	open fun queryWithSystemColumns(subquery: String): String {
		val query = """
      SELECT
      row_number() OVER (order by null) "PLAYSQL_ID",
      row_number() OVER (order by null) "PLAYSQL_POSITION",
      subquery.*
      FROM ( $subquery ) subquery
		"""
		return query
	}


	open fun readData(fieldlist: String,
				 limit: Long?,
				 offset: Long?,
				 order: String?,
				 tablename: String,
				 whereClause: String?): String {
		val query = """
      SELECT $fieldlist
      FROM $tablename AS TARGET_TABLE
      ${where(whereClause)}
      ${order(order)}
      ${limit(limit)}
      ${offset(offset)}
		"""
		return query
	}

	open fun writeData(fieldlist: List<String>, pkvalueint: Long, tableName: String): String {
		val query = """
      UPDATE $tableName
      SET ${fieldlist.joinToString(transform = { field -> "$field = ?" })}
      WHERE "ID" = $pkvalueint
		"""
		return query
	}

	open fun insertData(fieldlist: List<String>, tablename: String): String {
		val query = """
      INSERT INTO $tablename (
        ${fieldlist.joinToString(transform = { field -> "\"$field\""})}
      ) VALUES (
        ${fieldlist.joinToString(transform = { field -> "?" })}
      )
		"""
		return query
	}

	open fun getLastGeneratedId(): String {
		val query = """
        select IDENTITY()
		"""
		return query
	}

	open fun removeData(pkvalueint: Int?, tablename: String): String {
		val query = """
        DELETE FROM "$tablename"
        ${if (pkvalueint != null) "WHERE \"ID\" = $pkvalueint" else ""} 
		"""
		return query
	}

	open fun queryTotals(fieldlist: List<String>, subquery: String?, tablename: String?): String {
		val query = """
      SELECT ${fieldlist.joinToString()}
      FROM ${if (subquery != null) "( $subquery ) INTERNAL_RESULTS" else "$tablename"}
		"""
		return query
	}

	open fun hasTable(): String {
		val query = """
        select COUNT(*)
        from  INFORMATION_SCHEMA.TABLES
        where LOWER(TABLE_NAME) = LOWER(?)
              and TABLE_SCHEMA = CURRENT_SCHEMA
		"""
		return query
	}

	open fun getCommentOnTable(): String {
		val query = """
        select
              T.TABLE_NAME TABLENAME,
              C.COMMENT COM
        from
              INFORMATION_SCHEMA.TABLES T
        left join
              INFORMATION_SCHEMA.SYSTEM_COMMENTS C
              on T.TABLE_CATALOG = C.OBJECT_CATALOG
              and T.TABLE_SCHEMA = C.OBJECT_SCHEMA
              and T.TABLE_NAME = C.OBJECT_NAME
              and C.OBJECT_TYPE='TABLE'
        where
              TABLE_SCHEMA=CURRENT_SCHEMA
              and TABLE_NAME = ?
		"""
		return query
	}

	open fun getCommentOnColumn(): String {
		val query = """
        select
              COMMENT COM
        from
              INFORMATION_SCHEMA.SYSTEM_COMMENTS C
        where
              TABLE_SCHEMA = CURRENT_SCHEMA
              and TABLE_NAME = ?
              and COLUMN_NAME = ?
              and OBJECT_TYPE = 'COLUMN'
		"""
		return query
	}

	open fun setCommentOnTable(tablename: String, comment: String): String {
		val query = """
		COMMENT ON TABLE "$tablename" IS $comment
		"""
		return query
	}


	open fun createTable(columns: List<String>, table: String): String {
		val query = """
        CREATE TABLE "$table" (
            ${columns.joinToString()}
		)
		"""
		return query
	}

	open fun renameTable(newName: String, table: String): String {
		val query = """
		ALTER TABLE "$table" RENAME TO "$newName"
		"""
		return query
	}

	open fun copyTable(origin: String, table: String): String {
		val query = """
		SELECT * INTO "$table" FROM "$origin"
		"""
		return query
	}

	open fun getMaxId(table: String): String {
		val query = """
		SELECT MAX("ID") + 1 FROM "$table"
		"""
		return query
	}

	open fun createSequence(maxId: Int, seqName: String, table: String): String {
		val query = """
        CREATE SEQUENCE "$seqName" START WITH $maxId;
        ALTER TABLE "$table" ALTER COLUMN "ID" SET DEFAULT nextval('"$seqName"')
		"""
		return query
	}

	open fun dropTable(dropBehaviour: String, ifExists: Boolean, table: String): String {
		val query = """
    	DROP TABlE $table ${if (ifExists) "IF EXISTS" else ""} $dropBehaviour
		"""
		return query
	}

	open fun addColumn(beforeColumn: String?, columnDefinition: String, table: String): String {
		val query = """
        ALTER TABLE $table
        ADD COLUMN $columnDefinition
        ${helper("BEFORE", beforeColumn)}
		"""
		return query
	}

	open fun dropColumn(behaviour: String, column: String, table: String): String {
		val query = """
        ALTER TABLE "$table"
        DROP COLUMN "$column"
        $behaviour
		"""
		return query
	}

	open fun renameColumn(newColumn: String, originalColumn: String, table: String): String {
		val query = """
        ALTER TABLE "$table"
        RENAME COLUMN "$originalColumn"
        TO "$newColumn"
		"""
		return query
	}

	open fun updateSettings(key: String, list: String ): String{
		val query="""
		DO $$
		BEGIN
			IF EXISTS
				( SELECT 1
				  FROM   playsql_settings
				  WHERE  key = '$key'
				)
			THEN
				UPDATE playsql_settings
				SET value = '$list'
				WHERE key = '$key' ;
			ELSE
				INSERT INTO playsql_settings (category, key, value)
				Values (null, '$key', '$list');
			END IF ;
		END
	   $$ ;
		"""
		return query
	}

	open fun copyBinaryData(fromTable: String, fromColumn: String, fromId: Int, toTable: String, toColumn: String, toId: Int): String {
		val query = """
		UPDATE $toTable
		SET $toColumn = (SELECT $fromColumn FROM $fromTable WHERE "ID" = $fromId )
		WHERE "ID" = $toId
		"""
		return query
	}

	open fun alterColumn(columnDefinition: String, table: String): String {
		val query = """
        ALTER TABLE $table
        ALTER COLUMN $columnDefinition
		"""
		return query
	}

	open fun setCommentOnColumn(tablename: String, columnname: String, comment: String): String {
		val query = """
        COMMENT ON COLUMN $tablename.$columnname IS $comment
		"""
		return query
	}

	open fun fkAddToColumn(column: String, fkDefinition: String, table: String): String {
		val query = """
      ALTER TABLE $table
      ADD FOREIGN KEY ($column) $fkDefinition
		"""
		return query
	}

	open fun getAllFks() : String {

		val query = """
		SELECT source_table::regclass
		FROM pg_attribute target_attr, pg_attribute source_attr,
				(SELECT source_table, target_table, source_constraints[i] source_constraints, target_constraints[i] AS target_constraints
   				FROM
     				(SELECT conrelid as source_table, confrelid AS target_table, conkey AS source_constraints, confkey AS target_constraints,
					generate_series(1, array_upper(conkey, 1)) AS i
      				FROM pg_constraint
					WHERE contype = 'f'
     				)query1
 				 )query2
		WHERE   target_attr.attnum = target_constraints AND target_attr.attrelid = target_table AND
				source_attr.attnum = source_constraints AND source_attr.attrelid = source_table

		"""
		return query

	}
	open fun fkRemove(constraintName: String, table: String): String {
		val query = """
      ALTER TABLE $table
      DROP CONSTRAINT $constraintName
		"""
		return query
	}

	open fun listFks(selectOnSourceTable: Boolean, selectOnTargetTableAndColumn: Boolean): String {
		val query = """
        SELECT
            tc.constraint_name,
            tc.table_schema,
            tc.table_name,
            kcu.column_name,
            ccu.table_name AS foreign_table_name,
            ccu.column_name AS foreign_column_name,
            pgc.confupdtype,
			pgc.confdeltype,
			pgc.confmatchtype
		FROM
            information_schema.table_constraints AS tc
        JOIN information_schema.key_column_usage AS kcu
          ON tc.constraint_name = kcu.constraint_name
        JOIN information_schema.constraint_column_usage AS ccu
          ON ccu.constraint_name = tc.constraint_name
        JOIN pg_catalog.pg_constraint AS pgc
          ON pgc.conname = tc.constraint_name
        WHERE
            ${ "" /* We only recognize fks within the current schema */ }
            tc.table_schema = CURRENT_SCHEMA AND ccu.table_schema = CURRENT_SCHEMA
            ${if (selectOnSourceTable) " AND tc.table_name = ? " else ""}
            ${if (selectOnTargetTableAndColumn) " AND ccu.table_name = ? " else ""}
            AND tc.constraint_type = 'FOREIGN KEY'
		"""
		return query
	}
	open fun fkInsertMissingValues(table: String,
                                   column: String,
                                   targetTable: String,
                                   targetColumn: String,
                                   targetColumn2: String?,
                                   targetColumnType: String,
                                   targetColumnIsNumber: Boolean,
                                   columnIsText: Boolean
                                   ): String {
		val query = """
        INSERT INTO $targetTable ($targetColumn ${if (targetColumn2 != null) ", $targetColumn2" else ""})
        SELECT DISTINCT $column::$targetColumnType ${if (targetColumn2 != null) ", ' #' || $column" else ""}
        FROM $table
        WHERE
          $table.$column IS NOT NULL
          ${if (targetColumnIsNumber && columnIsText) " AND $table.$column ~ '^[0-9\\.]+$'" else ""}
          AND $table.$column::$targetColumnType NOT IN (SELECT $targetColumn FROM $targetTable WHERE $targetColumn IS NOT NULL)
        ORDER BY $column ASC
		"""
		return query
	}

    open fun fkReplaceValueWithIDs(table: String,
                                   columnDisplay: String,
                                   columnID: String,
                                   targetTable: String,
                                   targetColumnDisplay: String,
                                   targetColumnID: String
                                  ): String {

        // Only works with casts to ::TEXT or ::VARCHAR(), obviously
		return """
            UPDATE $table as mainquery
            SET $columnID =

                ( SELECT SUBTABLE.$targetColumnID
                        FROM $targetTable AS SUBTABLE
                        WHERE mainquery.$columnDisplay = SUBTABLE.$targetColumnDisplay )

            WHERE $columnDisplay IS NOT NULL
        """;
	}

	open fun listColumns(): String {
		val query = """
        select
              T.COLUMN_NAME, T.DATA_TYPE,
              T.COLUMN_DEFAULT, T.IS_NULLABLE, T.IS_GENERATED,
              T.IS_UPDATABLE, T.IS_IDENTITY,
              T.CHARACTER_MAXIMUM_LENGTH,
              T.NUMERIC_PRECISION, T.NUMERIC_SCALE,
              T.DATETIME_PRECISION,
              T.INTERVAL_TYPE, T.INTERVAL_PRECISION,
              C.COMMENT COM
        from
              INFORMATION_SCHEMA.COLUMNS T
        left join
              INFORMATION_SCHEMA.SYSTEM_COMMENTS C
              on T.TABLE_CATALOG = C.OBJECT_CATALOG
              and T.TABLE_SCHEMA = C.OBJECT_SCHEMA
              and T.TABLE_NAME = C.OBJECT_NAME
              and T.COLUMN_NAME = C.COLUMN_NAME
              and C.OBJECT_TYPE='COLUMN'
        where
              T.TABLE_NAME = ?
              and T.TABLE_SCHEMA = CURRENT_SCHEMA
        order by
              T.ORDINAL_POSITION
		"""
		return query
	}


	open fun selectTable(table: String): String {
		val query = """
			SELECT * FROM $table LIMIT 20
		"""
		return query
	}

	open fun createSettingsTable(): String {
		val query = """
            CREATE TABLE playsql_settings
            (
                id serial primary key,
                category text,
                key text,
                value text
            )
		"""
		return query
	}

	open fun getSettingsWithoutCategory(): String {
		val query = """
			SELECT value FROM playsql_settings WHERE key = ? AND category IS NULL
		"""
		return query
	}

	open fun getSettingsWithCategory(): String {
		val query = """
			SELECT value FROM playsql_settings WHERE key = ? AND category = ?
		"""
		return query
	}

	open fun deleteSettings(): String {
		val query = """
			DELETE FROM playsql_settings WHERE key = ?
		"""
		return query
	}

	open fun insertSettings(): String {
		val query = """
			INSERT INTO playsql_settings (category, key, value) VALUES (?, ?, ?)
		"""
		return query
	}

	open fun createFilesTable(): String {
		val query = """
    CREATE TABLE playsql_script_files
    (
        id serial primary key,
        version integer,
        scriptId integer references playsql_entities(id),
        type text,
        name text,
        content text,
        fileId integer
    )
		"""
		return query
	}

	open fun createFile(): String {
		val query = """
    		INSERT INTO playsql_script_files (version, scriptId, type, name, content, fileId)
    		VALUES (?, ?, ?, ?, ?, ?)
		"""
		return query
	}

	open fun updateFile(): String {
		val query = """
    		UPDATE playsql_script_files
    		SET version = ?,
    		    scriptId = ?,
    		    type = ?,
    		    name = ?,
    		    content = ?,
    		    fileId = ?
    		WHERE id = ?
		"""
		return query
	}

	open fun getFile(): String {
		val query = """
    		SELECT *
    		FROM playsql_script_files
    		WHERE id = ?
		"""
		return query
	}

	open fun deleteFile(): String {
		val query = """
			DELETE FROM playsql_script_files WHERE id = ?
		"""
		return query
	}

	open fun readFilesTable(): String {
		val query = """
			SELECT * FROM playsql_script_files
		"""
		return query
	}

	open fun getAllScriptFiles(): String {
		val query = """
    		SELECT *
    		FROM playsql_script_files
    		WHERE scriptId = ?
		"""
		return query
	}

	open fun createAuditTrailTable(): String {
		val query = """
    CREATE TABLE playsql_audit_trail
    (
        id serial primary key,
        operation text,
        table_name text,
        column_name text,
        row_id integer,
        author text,
        timestamp timestamp without time zone,
        comments text,
        data_type text,
        old_val text,
        new_val text
    )
		"""
		return query
	}

	open fun readAuditTrail(hasAuthor: Boolean,
					   hasColumnName: Boolean,
					   hasRowId: Boolean,
					   hasTableName: Boolean,
					   includeGeneralActivity: Boolean,
					   limit: Int,
					   offset: Int): String {
		val query = """
        	SELECT id, operation,
               table_name, column_name, row_id,
               author, timestamp, comments,
               data_type, old_val, new_val
        	FROM playsql_audit_trail
        	WHERE 1=1
            	${if (hasTableName) "AND table_name = ?" else ""}
				${if (hasColumnName) "AND column_name = ?" else ""}
				${if (hasRowId) "AND (row_id = ? ${if (includeGeneralActivity) "OR row_id IS NULL" else ""}" else ""}
				${if (hasAuthor) "AND author = ?" else ""}
				${if (includeGeneralActivity) "OR table_name IS NULL" else ""}
			ORDER BY timestamp DESC
			${limit(limit)}
			${offset(offset)}
		"""
		return query
	}

	open fun insertAuditTrail(): String {
		val query = """
        	INSERT INTO playsql_audit_trail
              (operation,
               table_name, column_name, row_id,
               author, timestamp, comments,
               data_type, old_val, new_val)
        	VALUES (?,
                ?, ?, ?,
                ?, ?, ?,
                ?, ?, ?)
		"""
		return query
	}

	open fun auditTrailTrimByDaysPrerequisites(): String {
		val query = """
            SELECT id, operation,
                table_name, column_name, row_id,
                author, timestamp, comments,
                data_type, old_val, new_val
            FROM playsql_audit_trail
            WHERE timestamp < NOW() - (INTERVAL '1 DAYS' * ?)
            AND (operation = 'COL_DROP' OR operation = 'TABLE_DROP')
            ORDER BY timestamp DESC 
		"""
		// We want table drops before column drops
		return query
	}

	open fun auditTrailTrimByDays(): String {
		val query = """
            DELETE FROM playsql_audit_trail
            WHERE timestamp < NOW() - (INTERVAL '1 DAYS' * ?)
            AND operation <> 'ACTIVATION'
		"""
		return query
	}

	open fun auditTrailTrimByCount1(): String {
		val query = """
            SELECT table_name
            FROM playsql_audit_trail
            GROUP BY table_name
            HAVING COUNT(*) >= ?
		"""
		return query
	}

	open fun auditTrailTrimByCountPrerequisites(): String {
		val query = """
            SELECT id, operation,
                table_name, column_name, row_id,
                author, timestamp, comments,
                data_type, old_val, new_val
            FROM playsql_audit_trail
            WHERE id IN (SELECT id FROM playsql_audit_trail WHERE table_name = ? ORDER BY timestamp DESC OFFSET ?)
            AND (operation = 'COL_DROP' OR operation = 'TABLE_DROP')
		"""
		return query
	}

	open fun auditTrailTrimByCount2(): String {
		val query = """
            DELETE FROM playsql_audit_trail
            WHERE id IN (SELECT id FROM playsql_audit_trail WHERE table_name = ? ORDER BY timestamp DESC OFFSET ?)
		"""
		return query
	}

	open fun createFormulaDependenciesTable(): String {
		val query = """
            CREATE TABLE playsql_formula_dependencies
            (
                id serial PRIMARY KEY,
                origin_tablename text,
                origin_columnname text,
                origin_rowid integer,
                destination_tablename text,
                destination_columnname text,
                destination_rowid integer,
                formula text
            )
		"""
		return query
	}

	open fun insertFormulaDependency(): String {
		val query = """
            INSERT INTO playsql_formula_dependencies
            ( origin_tablename, origin_columnname, origin_rowid,
              destination_tablename, destination_columnname, destination_rowid,
              formula)
            VALUES ( ?, ?, ?,
                     ?, ?, ?,
                     ?)
		"""
		return query
	}

	open fun listFormulaDependencies(MAX_RECURSION: Long,
								cascade: Boolean,
								colNameIsNull: Boolean,
								includeSelf: Boolean,
								rowIdIsNull: Boolean,
								tableNameIsNull: Boolean): String {
		val query = if (cascade) """
        	WITH RECURSIVE deps(tablename, columnname, rowid, formula, level) AS (

                SELECT destination_tablename, destination_columnname, destination_rowid, formula, 1 AS level
                FROM playsql_formula_dependencies
                WHERE 1=1
				${if (!tableNameIsNull) "AND origin_tablename = ?" else ""}
				${if (!colNameIsNull) "AND (origin_columnname = ? OR origin_columnname IS NULL)" else ""}
				${if (!rowIdIsNull) "AND (origin_rowid = ? OR origin_rowid IS NULL)" else ""}

            UNION ALL
                SELECT
                deps2.destination_tablename, deps2.destination_columnname, deps2.destination_rowid, deps2.formula, deps.level+1
                FROM playsql_formula_dependencies deps2
                JOIN deps ON deps2.origin_tablename = deps.tablename
                AND (deps2.origin_columnname = deps.columnname OR deps2.origin_columnname IS NULL)
                AND (deps2.origin_rowid = deps.rowid OR deps2.origin_rowid IS NULL)
                AND level <= $MAX_RECURSION

            )
            SELECT DISTINCT tablename, columnname, rowid, formula, MAX(level) AS level FROM deps
            GROUP BY tablename, columnname, rowid, formula
            ${if (includeSelf) """

                UNION ALL

                SELECT DISTINCT
                    destination_tablename tablename, destination_columnname columnname, destination_rowid rowid, formula, 0 AS level
                FROM
                    playsql_formula_dependencies
                WHERE 1=1
				${if (!tableNameIsNull) "AND destination_tablename = ?" else ""}
				${if (!colNameIsNull) "AND destination_columnname = ?" else ""}
				${if (!rowIdIsNull) "AND destination_rowid = ?" else ""}

            """ else ""}
            ORDER BY level ASC
		"""
		else """
			SELECT DISTINCT
                destination_tablename tablename, destination_columnname columnname, destination_rowid rowid, formula, 1 AS level
            FROM
                playsql_formula_dependencies
            WHERE 1=1
			${if (!tableNameIsNull) "AND origin_tablename = ?" else ""}
			${if (!colNameIsNull) "AND origin_tablename = ?" else ""}
			${if (!rowIdIsNull) "AND (origin_rowid = ? OR origin_rowid IS NULL)" else ""}
		"""
		return query
	}

	open fun formulaDependendenciesFindLoop(MAX_RECURSION: Long,
									   cascade: Boolean,
									   colNameIsNull: Boolean,
									   rowIdIsNull: Boolean,
									   tableNameIsNull: Boolean): String {
		val query = """

            -- This query is in charge of searching for recursive loops. It stores each step in an array ("path")
            -- then the 'loop_detected' column tells uses the x==ANY(array) condition to check for presence
            -- of the current formula
            WITH RECURSIVE deps(tablename, columnname, rowid, formula, level, path, loop_detected) AS (

                SELECT
                    destination_tablename, destination_columnname, destination_rowid, formula,
                    1 AS level,
                    ARRAY[destination_tablename || '.' || destination_columnname || ':' || destination_rowid] path,
                    FALSE loop_detected
                FROM playsql_formula_dependencies
                WHERE 1=1
                ${if (!tableNameIsNull) "AND origin_tablename = ?" else ""}
				${if (!colNameIsNull) "AND (origin_columnname = ? OR origin_columnname IS NULL)" else ""}
				${if (!rowIdIsNull) "AND (origin_rowid = ? OR origin_rowid IS NULL)" else ""}

			UNION ALL
                SELECT
                    deps2.destination_tablename, deps2.destination_columnname, deps2.destination_rowid, deps2.formula,
                    deps.level+1,
                    path || (deps2.destination_tablename || '.' || deps2.destination_columnname || ':' || deps2.destination_rowid),
                    ((deps2.destination_tablename || '.' || deps2.destination_columnname || ':' || deps2.destination_rowid) = ANY(path)) loop_detected
                FROM playsql_formula_dependencies deps2
                JOIN deps ON deps2.origin_tablename = deps.tablename
                AND (deps2.origin_columnname = deps.columnname OR deps2.origin_columnname IS NULL)
                AND (deps2.origin_rowid = deps.rowid OR deps2.origin_rowid IS NULL)
                AND level <= $MAX_RECURSION

            )
            SELECT formula, level, loop_detected, ARRAY_TO_STRING(path, ' -> ') path
            FROM deps
            WHERE loop_detected
            LIMIT 1
		"""
		return query
	}

	open fun removeFormulaDependencies(colNameIsNull: Boolean,
								  rowIdIsNull: Boolean,
								  tableNameIsNull: Boolean): String {
		val query = """
            DELETE
            FROM
                playsql_formula_dependencies
            WHERE 1=1
            ${if (!tableNameIsNull) "AND destination_tablename = ?" else ""}
			${if (!colNameIsNull) "AND (destination_columnname = ? OR destination_columnname IS NULL)" else ""}
			${if (!rowIdIsNull) "AND (destination_rowid = ? OR destination_rowid IS NULL)" else ""}
		"""
		return query
	}

	open fun executeFormula(columnName: String,
					   formula: String,
					   tableName: String): String {
		val query = """
            UPDATE $tableName
            SET $columnName = ($formula)
            WHERE "ID" = ?
		"""
		return query
	}

	open fun updateFormula(): String {
		val query = """
            UPDATE playsql_formula_dependencies
            SET formula = ?
            WHERE destination_tablename = ?
            AND destination_columnname = ?
            AND destination_rowid = ?
		"""
		return query
	}

	open fun updateFormulaRenameColumnOrigin(): String {
		val query = """
            UPDATE playsql_formula_dependencies
            SET origin_columnname = ?
            WHERE origin_tablename = ?
            AND origin_columnname = ?
		"""
		return query
	}

	open fun updateFormulaRenameColumnDestination(): String {
		val query = """
            UPDATE playsql_formula_dependencies
            SET destination_columnname = ?
            WHERE destination_tablename = ?
            AND destination_columnname = ?
		"""
		return query
	}


	open fun createEntitiesTable(): String {
		val query = """
            CREATE SEQUENCE playsql_entities_id_seq
            START WITH 1000;
            CREATE TABLE playsql_entities
            (
                id integer NOT NULL DEFAULT nextval('playsql_entities_id_seq') PRIMARY KEY,
                entity_type text,
                key text,
                label text,
                last_modified text,
                sql text,
                serialized text
            );
            ALTER SEQUENCE playsql_entities_id_seq OWNED BY playsql_entities.id;
		"""
		return query
	}

	open fun insertEntity(): String {
		val query = """
            INSERT INTO playsql_entities (entity_type, key, label, last_modified, sql, serialized)
            VALUES (?, ?, ?, ?, ?, ?)
		"""
		return query
	}

	open fun readEntity(hasId: Boolean,
				        hasKey: Boolean,
				        hasType: Boolean,
                        hasSerialized: Boolean): String {
		val query = """
            SELECT id, entity_type, label ${if (hasSerialized) " , serialized" else "" }
            FROM playsql_entities
            WHERE ${if (hasType) "entity_type = ?" else "1=1"}
			${if (hasId) "AND id = ?" else ""}
			${if (hasKey) "AND COALESCE(key, id::text) = ?" else ""}
		"""
		return query
	}

	open fun deleteEntity(hasId: Boolean,
					 hasType: Boolean): String {
		val query = """
            DELETE FROM playsql_entities
            WHERE
            ${if (hasType) "entity_type = ?" else "1=1"}
			${if (hasId) "AND id = ?" else ""}
		"""
		return query
	}

	open fun updateEntity(hasId: Boolean,
					 hasType: Boolean): String {
		val query = """
            UPDATE playsql_entities
            SET entity_type = ?,
                key = ?,
                label = ?,
                last_modified = ?,
                sql = ?,
                serialized = ?
            WHERE
            ${if (hasType) "entity_type = ?" else "1=1"}
			${if (hasId) "AND id = ?" else ""}
		"""
		return query
	}

	open fun usersList(detail: Boolean): String {
		val query = """
            SELECT rolname username
            ${if (detail) """
				,
                   ARRAY(SELECT parent.rolname
                         FROM pg_roles parent
                         JOIN pg_auth_members m ON u.oid = m.member
                         WHERE parent.oid = m.roleid
                   ) memberships
            """ else ""}
			FROM pg_roles u
            WHERE rolname LIKE ? || '%'
		"""
		return query
	}

	open fun userCreate(userName: String,
				   userPassword: String?): String {
		val query = """
            CREATE USER $userName
            ${if (userPassword != null) "WITH ENCRYPTED PASSWORD $userPassword" else ""}
		"""
		return query
	}

	open fun userUpdatePassword(userName: String,
						   userPassword: String): String {
		val query = """
            ALTER USER $userName
            WITH ENCRYPTED PASSWORD $userPassword
		"""
		return query
	}

	open fun userExists(): String {
		val query = """
            SELECT COUNT(*) FROM pg_user WHERE usename = ?
		"""
		return query
	}

	open fun userDrop(userName: String): String {
		val query = """
            DROP USER $userName
		"""
		return query
	}

	open fun databaseCreate(dbName: String): String {
		val query = """
            CREATE DATABASE $dbName
		"""
		return query
	}

	open fun databaseExists(): String {
		val query = """
            SELECT COUNT(*) FROM PG_DATABASE WHERE datname = ?
		"""
		return query
	}

	open fun databaseDrop(dbName: String): String {
		val query = """
            DROP DATABASE $dbName
		"""
		return query
	}

	open fun databaseGrantAll(dbName: String,
						 dbUser: String): String {
		val query = """
            GRANT ALL PRIVILEGES ON DATABASE $dbName
            TO $dbUser
		"""
		return query
	}

	open fun databaseList(): String {
		val query = """
            SELECT
                datname AS database_name,
                pg_database_size(datname) AS size,
                (SELECT usename FROM pg_catalog.pg_user u WHERE u.usesysid = d.datdba LIMIT 1) AS owner,
                datallowconn,
                datconnlimit,
                datacl permissions
            FROM pg_catalog.pg_database d
            ORDER BY size DESC;
		"""
		return query
	}

	open fun databaseSetAllowcon(dbName: String, status: Boolean): String {
		val query = """
            UPDATE pg_catalog.pg_database
            SET datallowconn = $status
            WHERE datname = $dbName;
		"""
		return query
	}

}
