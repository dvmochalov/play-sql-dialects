package com.playsql.jdbc.dialect;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.api.DDLDialect;
import com.playsql.dialect.api.DialectInfo;
import com.playsql.jdbc.dialect.templates.TemplatesForOracle;
import com.playsql.spi.CacheManager;
import com.playsql.spi.SpiUtils;
import com.playsql.spi.TemplateEngine;

import java.util.Locale;

import static com.playsql.dialect.api.DDLDialectMetadata.CaseSensibility.LOWER;

@DialectInfo(
        key = "com.playsql.jdbc.dialect.OracleDDLDialect",
        i18n = "Oracle Unsupported",
        defaultCase = LOWER,
        readWrite = false,
        autocomplete = false,
        monitoring = false,
        driverName = "oracle.jdbc.OracleDriver",
        exampleUrl = "jdbc:oracle:thin:@localhost:1521/orcl",
        minimumDriverString = "oracle",
        minimumUrlString = "oracle"
)
public class OracleDDLDialect extends GenericDDLDialect implements DDLDialect
{
    @Override
    public String escapeEntityName(String name) {
        return name.toLowerCase(Locale.ENGLISH).replaceAll("[^a-z0-9]", "_").replaceAll("^[_0-9]*", "");
    }

    public OracleDDLDialect(TemplateEngine templateEngine, SpiUtils spiUtils, CacheManager cacheManager) {
        super(spiUtils, cacheManager);
    }

    @Override
    public TemplatesForOracle templates() {
        return new TemplatesForOracle();
    }
}