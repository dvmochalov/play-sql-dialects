package com.playsql.jdbc.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.model.Warehouse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class helps jsonize the metadata about a schema, to store it in
 * the schema comments.
 *
 * <p/>
 * Since dialects are free to store metadata in the schema comments or in a
 * system table, this class must stay together with <i>implementations</i> of
 * dialects.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SchemaComment {

    /** The warehouse key matches the context's string representation */
    private String key;

    /** The label */
    private String label;

    /** The avatar image, so we can display the sidebar */
    private String avatarUrl;

    /** The name of the space */
    private String spaceLabel;

    public SchemaComment() {
    }

    public SchemaComment(Warehouse warehouse) {
        this.key = warehouse.getKey();
        this.label = warehouse.getLabel();
        this.avatarUrl = warehouse.getAvatarUrl();
        this.spaceLabel = warehouse.getSpaceLabel();
    }

    public SchemaComment(String key, String label, String avatarUrl, String spaceLabel) {
        this.key = key;
        this.label = label;
        this.avatarUrl = avatarUrl;
        this.spaceLabel = spaceLabel;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getSpaceLabel() {
        return spaceLabel;
    }

    public void setSpaceLabel(String spaceLabel) {
        this.spaceLabel = spaceLabel;
    }
}
