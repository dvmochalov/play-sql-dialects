
Lists of 6 third-party dependencies.
     (HSQLDB License, a BSD open source license) HyperSQL Database (org.hsqldb:hsqldb:2.2.9 - http://hsqldb.org)
     (Apache License 2.0) IntelliJ IDEA Annotations (org.jetbrains:annotations:13.0 - http://www.jetbrains.org)
     (The Apache License, Version 2.0) org.jetbrains.kotlin:kotlin-stdlib (org.jetbrains.kotlin:kotlin-stdlib:1.1.4-2 - https://kotlinlang.org/)
     (The Apache License, Version 2.0) org.jetbrains.kotlin:kotlin-stdlib-jre7 (org.jetbrains.kotlin:kotlin-stdlib-jre7:1.1.4-2 - https://kotlinlang.org/)
     (The Apache License, Version 2.0) org.jetbrains.kotlin:kotlin-stdlib-jre8 (org.jetbrains.kotlin:kotlin-stdlib-jre8:1.1.4-2 - https://kotlinlang.org/)
     (The PostgreSQL License) PostgreSQL JDBC Driver (org.postgresql:postgresql:9.4-1201-jdbc41 - http://jdbc.postgresql.org)
