/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Function;
import com.playsql.spi.CacheManager;
import com.playsql.spi.utils.Option;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MockCacheManager implements CacheManager {

    private static class MockCache<T extends Serializable> implements Cache<T> {
        private final Map<String, Object> contents = new HashMap<String, Object>();
        private final Class<T> clazz;
        private final boolean nullValueAllowed;
        private final Function<T, T> cloner;

        private MockCache(Class<T> clazz, boolean nullValueAllowed, Function<T, T> cloner) {
            this.clazz = clazz;
            this.nullValueAllowed = nullValueAllowed;
            this.cloner = cloner;
        }

        private T clone(T t) {
            return cloner != null ? cloner.apply(t) : t;
        }

        @Override
        public T get(String key) {
            return get(key, null);
        }

        @Override
        public T get(String key, T defaultValue) {
            Object value = contents.get(key);
            if (nullValueAllowed) {
                if (value instanceof Option) {
                    value = ((Option) value).get();
                    if (clazz.isInstance(value)) {
                        return clone((T) value);
                    } else if (value == null) {
                        return null;
                    }
                }
            } else if (clazz.isInstance(value)) {
                // The difference is, we don't return null if it's not instanceof here
                return clone((T) value);
            }
            // The object wasn't found
            return clone(defaultValue);
        }

        public void put(String key, T value) {
            contents.put(key, value);
        }

        @Override
        public T getOrSet(String key, InitialValue<? extends T> initializer) {
            Object value = contents.get(key);
            if (nullValueAllowed) {
                if (value instanceof Option) {
                    value = ((Option) value).get();
                    if (clazz.isInstance(value)) {
                        return clone((T) value);
                    } else if (value == null) {
                        return null;
                    }
                }
            } else if (clazz.isInstance(value)) {
                // The difference is, we don't return null if it's not instanceof here
                return clone((T) value);
            }
            // The object wasn't found

            value = initializer.generate();
            if (nullValueAllowed) {
                contents.put(key, new Option(value));
            } else {
                contents.put(key, value);
            }
            return clone((T) value);
        }

        @Override
        public boolean removeAll() {
            contents.clear();
            return true;
        }

        @Override
        public boolean removeAllAll() {
            contents.clear();
            return true;
        }

        @Override
        public boolean remove(String key) {
            contents.remove(key);
            return true;
        }

    }

    @Override
    public <T extends Serializable> Cache<T> getCache(String cacheName, Class<T> clazz) {
        return new MockCache<>(clazz, false, null);
    }

    @Override
    public <T extends Serializable> Cache<T> getCache(String cacheName, Class<T> clazz, boolean hasNullValues, Function<T, T> cloner) {
        return new MockCache<>(clazz, hasNullValues, cloner);
    }

    @Override
    public void removeAll() {
        throw new NotImplementedException("MockCacheManager#removeAll()");
    }
}
