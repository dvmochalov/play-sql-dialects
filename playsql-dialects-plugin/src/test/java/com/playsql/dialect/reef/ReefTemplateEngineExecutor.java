package com.playsql.dialect.reef;/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.File;
import java.net.URL;

import static com.playsql.spi.utils.PlaySqlUtils.equal;

public class ReefTemplateEngineExecutor {

    private File scopePath = new File(".");

    public static void main(String[] args) {
        System.out.println("Transforms Reef files into Java files");
        System.out.println("Usage: No arguments");
        // Get the dialect plugin's root path
        URL descriptorResource = ReefTemplateEngineExecutor.class.getResource(".");
        if (descriptorResource == null) throw new RuntimeException("Unable to guess the module directory.");
        String srcMainResourcesDirPath = descriptorResource.getFile();
        if (srcMainResourcesDirPath == null) throw new RuntimeException("Unable to guess the module directory.");
        File scopePath = new File(srcMainResourcesDirPath);
        while (scopePath != null && !equal(scopePath.getName(), "playsql-dialects-plugin"))
            scopePath = scopePath.getParentFile();
        if (scopePath == null || !scopePath.exists() || !scopePath.isDirectory())
            throw new RuntimeException("Unable to guess the module directory: " + srcMainResourcesDirPath);
        new ReefTemplateEngineExecutor().setScopePath(scopePath).run();
    }

    @Test
    public void run() {
        ReefTemplateEngine engine = new ReefTemplateEngine();
        File dir = new File(scopePath, "src/main/resources");
        if (!dir.exists() || !dir.isDirectory()) {
            throw new RuntimeException("Directory doesn't exist, check your execution directory: " + dir.getAbsolutePath());
        }
        File destinationDir = new File(scopePath, "src/main/java/com/playsql/jdbc/dialect/templates");
        destinationDir.mkdirs();
        for (File reefFile : dir.listFiles(path -> path.isFile() && path.getName().endsWith(".reef"))) {
            String destinationName = StringUtils.removeEnd(reefFile.getName(), ".reef");
            File destinationFile = new File(destinationDir, destinationName);
            engine.transform(reefFile, destinationFile);
        }
    }

    public ReefTemplateEngineExecutor setScopePath(File scopePath) {
        this.scopePath = scopePath;
        return this;
    }
}
