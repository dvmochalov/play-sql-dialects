package com.playsql.dialect.reef;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.playsql.spi.utils.PlaySqlUtils.equal;


public class ReefTemplateEngine {

    public static final String INDENT_2 = "\n        ";
    public static final String INDENT_3 = "\n            ";

    static abstract class State {
        protected List<Object> elements = Lists.newArrayList();
        // List of variables that are defined at this level.
        protected List<Pair<String, String>> variables = Lists.newArrayList();
        protected State parent;

        public State(State parent) {
            this.parent = parent;
        }

        public void consume(String portion) {
            elements.add(portion);
        }

        public int start(String endOfLine) {
            return 0;
        }

        public State end() {
            if (parent == null) {
                throw new RuntimeException("Missing end for " + this.getClass().getName());
            }
            //parent.elements.add(this);
            return parent;
        }

        public void declare(String variable, String type) {
            if (Iterables.find(variables, el -> equal(el.getLeft(), variable), null) == null) {
                if (parent != null) {
                    parent.declare(variable, type);
                } else {
                    variables.add(Pair.of(variable, type));
                }
            }
        }

        public void eol() {
            elements.add("\n");
        }

        protected String getRealNameForVariable(String variableNameWithDollar) {
            if (equal(variableNameWithDollar, "$foreachCounter")) {
                State p = parent;
                while (p != null)
                    if (p instanceof ForeachState)
                        return ((ForeachState) p).iteratorCounter;
            }
            return variableNameWithDollar.substring(1);
        }

        @Override
        public String toString() {
            return String.format("%s%s{%s} %s",
                this.getClass().getSimpleName(),
                variables.isEmpty() ? "" : "(" + StringUtils.join(variables, ",") + ")",
                StringUtils.join(elements, ", "),
                parent != null ? "-> " + parent.toString() : "");
        }
    }

    static class RootState extends State {

        public RootState(BufferedWriter writer) {
            super(null);
            elements = new ArrayList<Object>(){
                @Override
                public boolean add(Object o) {
                    try {
                        writer.write(java.util.Objects.toString(o));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    return true;
                }

                @Override
                public boolean addAll(Collection<?> collection) {
                    for (Object el : collection)
                        add(el);
                    return true;
                }
            };
        }

        @Override
        public State end() {
            elements.add("\n}\n");
            return null;
        }
    }

    static class MethodState extends State {
        private static final Pattern METHOD_PATTERN = Pattern.compile("^\\s+([a-zA-Z0-9_]+)(\\s*\\(([a-zA-Z0-9 _<>,]*)\\))?\\s*(=\\s*)?");
        private static final Pattern ARGUMENT_PATTERN = Pattern.compile("\\s*([a-zA-Z0-9_<>]+)\\s*([a-zA-Z0-9_]+)\\s*");
        private String methodName;

        public MethodState(State parent) {
            super(parent);
        }

        public int start(String endOfLine) {
            Matcher matcher = METHOD_PATTERN.matcher(endOfLine);
            if (!matcher.find()) {
                throw new RuntimeException("Parsing failed for @method" + endOfLine);
            }
            methodName = matcher.group(1);
            String argumentsPart = matcher.group(3);
            if (argumentsPart != null) {
                Matcher argumentMatcher = ARGUMENT_PATTERN.matcher(argumentsPart);
                while (argumentMatcher.find()) {
                    String type = argumentMatcher.group(1);
                    String name = argumentMatcher.group(2);
                    variables.add(Pair.of(name, type));
                }
            }
            return matcher.end();
        }

        @Override
        public void declare(String variable, String type) {
            // As opposed to any other type, variables in methods are all parameters
            if (Iterables.find(variables, var -> equal(var.getKey(), variable), null) == null) {
                variables.add(Pair.of(variable, type));
            }
        }

        public String getMethodName() {
            return methodName;
        }

        @Override
        public State end() {
            StringBuilder output = new StringBuilder();
            output.append("    public String ").append(methodName).append("(");
            int indentationForArguments = output.length();
            boolean first = true;
            for (Pair<String, String> arg : Sets.newTreeSet(variables)) {
                if (first) {
                    first = false;
                } else {
                    output.append(", \n").append(StringUtils.repeat(" ", indentationForArguments));
                }
                output.append(arg.getRight() + " " + arg.getLeft());
            }
            output.append(") {")
                .append(INDENT_2).append("StringBuilder sb = new StringBuilder()");
            int crlfCount = 0;
            boolean isSbOpen = true;
            for (Object line : elements) {
                if (line instanceof String) {
                    if (line.equals("\n")) {
                        if (output.length() != 0)
                            crlfCount++;
                    } else if (StringUtils.isNotBlank((String) line)) {
                        if (isSbOpen) {
                            if (crlfCount > 0) { output.append(INDENT_3).append(".append(\""); } else { output.append(".append(\""); }
                        } else {
                            output.append(INDENT_2).append("sb.append(\"");
                            isSbOpen = true;
                        }

                        while (crlfCount > 0) {
                            output.append("\\n");
                            crlfCount--;
                        }
                        output.append(javaEscape(line)).append("\")");
                    }
                } else if (line instanceof LiteralCode) {
                    if (isSbOpen) {
                        output.append(";");
                        isSbOpen = false;
                    }
                    output.append(INDENT_2).append(StringUtils.join(StringUtils.split(((LiteralCode) line).getCode(), '\n'), INDENT_2));
                } else {
                    if (isSbOpen) {
                        output.append(INDENT_3).append(".append(").append(line).append("\")\n");
                    } else {
                        output.append(INDENT_2).append("sb.append(").append(line).append("\")\n");
                        isSbOpen = true;
                    }
                }
            }
            if (isSbOpen) output.append(";");
            output.append(INDENT_2).append("return sb.toString();");
            output.append("\n    }\n\n");
            if (parent != null) {
                parent.elements.add(output.toString());
            } else {
                this.elements = Lists.newArrayList(output.toString());
            }
            return super.end();
        }

        private String javaEscape(Object line) {
            return ESCAPE_JAVA.matcher((String) line).replaceAll("\\\\$0");
        }
    }
    private final static Pattern ESCAPE_JAVA = Pattern.compile("[\\\\\"]");

    static class ForeachState extends State {
        private static final Pattern FOREACH_PATTERN = Pattern.compile("^\\s+\\(\\$([a-zA-Z0-9_]+)\\s+in\\s+\\$([a-zA-Z0-9_]+)\\)");
        private String iteratorCounter;
        private String iteratorVariable;
        private String listVariable;

        public ForeachState(State parent) {
            super(parent);
            int nesting = 0;
            State parents = parent;
            while (parents != null) {
                if (parents instanceof ForeachState) nesting++;
                parents = parents.parent;
            }
            iteratorCounter = new String[] {"i", "j", "k", "l"}[nesting];
        }

        public int start(String endOfLine) {
            Matcher matcher = FOREACH_PATTERN.matcher(endOfLine);
            if (!matcher.find()) {
                throw new RuntimeException("Parsing failed for @foreach" + endOfLine);
            }
            iteratorVariable = matcher.group(1);
            listVariable = matcher.group(2);
            parent.declare(listVariable, "List<Object>");
            this.variables.add(Pair.of(iteratorVariable, null));
            this.variables.add(Pair.of("foreachCounter", null));
            this.variables.add(Pair.of(iteratorCounter, null));
            elements.add(new LiteralCode(String.format("for (int %s = 0; %s < %s.size(); %s++) {\n    Object %s = %s.get(%s);",
                iteratorCounter, iteratorCounter, listVariable, iteratorCounter,
                iteratorVariable, listVariable, iteratorCounter)));
            return matcher.end();
        }

        @Override
        public State end() {
            elements.add(new LiteralCode("}"));
            parent.elements.addAll(elements);
            return super.end();
        }
    }

    static class IfState extends State {

        Pattern CONDITION_PATTERN = Pattern.compile("\\((([^()]*|\\([^\\(\\)]\\))+)\\)");
        Pattern VARIABLES = Pattern.compile("\\$([a-zA-Z0-9_]+)");

        public IfState(State parent) {
            super(parent);
        }

        @Override
        public int start(String endOfLine) {
            Matcher matcher = CONDITION_PATTERN.matcher(endOfLine);
            if (!matcher.find()) {
                throw new RuntimeException("Can't find the condition in: " + endOfLine);
            }
            String condition = matcher.group();

            Matcher variableExtractor = VARIABLES.matcher(condition);
            while (variableExtractor.find()) {
                String variableName = variableExtractor.group();
                String realName = getRealNameForVariable(variableName);
                // Replaces $name with name, and $foreachCounter with i.
                condition = condition.replace(variableExtractor.group(), realName);
                parent.declare(realName, "Object");
            }
            elements.add(new LiteralCode("if " + condition + " {"));
            return matcher.end();
        }

        public void doElse() {
            elements.add(new LiteralCode("\n} else {"));
            parent.elements.addAll(elements);
            elements.clear();
        }

        @Override
        public State end() {
            elements.add(new LiteralCode("\n}"));
            parent.elements.addAll(elements);
            return super.end();
        }

        @Override
        public void declare(String variable, String type) {
            super.declare(variable, type);
        }

        @Override
        public void eol() {
            super.eol();
        }
    }

    static class CommentState extends State {

        public CommentState(State parent) {
            super(parent);
        }

        @Override
        public State end() {
            StringBuilder comment = new StringBuilder();

            comment.append("\n    /**\n     * ");
            for (Object el : elements) {
                el.toString().replace("\n", "\n     * ");
                comment.append(el.toString());
            }
            comment.append("\n    **/");
            parent.elements.add(new LiteralCode(comment.toString()));
            return parent;
        }
    }

    static class VariableState extends State {

        private String variableName;

        public VariableState(State parent) {
            super(parent);
        }

        @Override
        public void consume(String portion) {
            throw new RuntimeException("Variables don't have a body. end() should be called immediately after creation.");
        }

        @Override
        public int start(String variableName) {
            this.variableName = getRealNameForVariable("$" + variableName);
            parent.elements.add(new LiteralCode("sb.append(" + this.variableName + ");"));
            parent.declare(this.variableName, "Object");
            return 0;
        }
    }

    static class LiteralState extends State {

        public LiteralState(State parent) {
            super(parent);
        }

        @Override
        public State end() {
            LiteralCode code = new LiteralCode(StringUtils.join(this.elements));
            super.elements.add(code);
            return super.end();
        }
    }

    static class LiteralCode {
        private final String code;

        LiteralCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public String toString() {
            return "{literal}" + code + "{/literal}";
        }
    }

    private static final Pattern TOKEN = Pattern.compile("(@[a-zA-Z0-9@_-]+|@\\*|\\*@)|\\$([a-zA-Z0-9@_]+)");

    public void transform(File reefFile, File destinationFile) {
        int lineNumber = 0;
        try (BufferedWriter writer = Files.newBufferedWriter(destinationFile.toPath(), Charset.defaultCharset())) {
            try (BufferedReader br = new BufferedReader(new FileReader(reefFile))) {
                String line;
                State state = new RootState(writer);
                while ((line = br.readLine()) != null) {
                    long startTime = System.nanoTime();
                    //if (line.startsWith("@comment")) continue;
                    Matcher matcher = TOKEN.matcher(line);
                    int consumed = 0;
                    while (matcher.find(consumed)) {
                        String portion = line.substring(consumed, matcher.start());
                        state.consume(portion);
                        String keyword = matcher.group(1);
                        consumed = matcher.end();
                        if (keyword != null) {
                            switch (keyword) {
                                case "@comment":
                                    consumed = line.length();
                                    break;
                                case "@methodOverride":
                                case "@method":
                                    if (state instanceof MethodState)
                                        state = state.end();
                                    if (equal(keyword, "@methodOverride"))
                                        state.consume("\n    @Override\n");
                                    state = new MethodState(state);
                                    consumed += state.start(line.substring(consumed));
                                    break;
                                case "@foreach":
                                    state = new ForeachState(state);
                                    consumed += state.start(line.substring(consumed));
                                    break;
                                case "@if":
                                    state = new IfState(state);
                                    consumed += state.start(line.substring(consumed));
                                    break;
                                case "@literal":
                                    state = new LiteralState(state);
                                    break;
                                case "@else":
                                    if (state instanceof IfState) {
                                        ((IfState) state).doElse();
                                    } else {
                                        throw new RuntimeException("Illegal @else when unexpected");
                                    }
                                    break;
                                case "@end":
                                    state = state.end();
                                    break;
                                default:
                                    if (keyword.startsWith("@@")) {
                                        state.consume("@");
                                        consumed += 2 - matcher.group().length();
                                    } else if (keyword.startsWith("@*")) {
                                        state = new CommentState(state);
                                        consumed += 2 - matcher.group().length();
                                        consumed += state.start(line.substring(consumed));
                                    } else if (keyword.startsWith("*@")) {
                                        consumed += 2 - matcher.group().length();
                                        state = state.end();
                                    } else {
                                        new NotImplementedException(keyword);
                                    }
                                    break;
                            }
                        } else {
                            // It's a variable !
                            String variableName = matcher.group(2);
                            if (variableName == null) throw new RuntimeException(line);
                            state = new VariableState(state);
                            consumed += state.start(variableName);
                            state = state.end();
                        }
                    }
                    if (consumed < line.length())
                        state.consume(line.substring(consumed));
                    state.eol();

                    System.out.format("%n%d (%dms): %s", ++lineNumber, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime), line);
                }
                while (state != null) {
                    state = state.end();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
