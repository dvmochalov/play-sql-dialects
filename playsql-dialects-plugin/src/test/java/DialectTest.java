/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.playsql.dialect.api.CapabilityEntities;
import com.playsql.dialect.api.DDLDialect;
import com.playsql.dialect.api.Jdbc;
import com.playsql.dialect.exceptions.CircularFormulaException;
import com.playsql.dialect.model.DDLColumn;
import com.playsql.dialect.model.DDLTable;
import com.playsql.dialect.model.ForeignKey;
import com.playsql.dialect.model.formulas.Endpoint;
import com.playsql.dialect.model.formulas.FormulaDependency;
import com.playsql.jdbc.dialect.PostgreSQLDDLDialect;
import com.playsql.spi.SpiUtils;
import com.playsql.spi.models.Tuple;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * This class currently only tests a subset of the dialects. If you develop your own dialect
 * and want to submit it, please help improving this class in order to check your own submission.
 * <p/>
 * Also, please submit an SQL file to import data before a test, for example with MySQL or others.
 */
@Ignore
public class DialectTest {
    private static Logger log = LoggerFactory.getLogger(DialectTest.class);
    private static SpiUtils spiUtils = new MockSpiUtils("admin");
    private static ExecutorService executor = Executors.newCachedThreadPool();

    private DDLDialect dialect;
    private DDLDialect rDialect;
    private SimpleDriverDataSource ds;
    private Jdbc jdbcWrapper;

    @Before
    public void setup() throws IOException {
        applyVendorSpecificInitialisation();
    }

    protected void applyVendorSpecificInitialisation() throws IOException {
        dialect = new PostgreSQLDDLDialect(spiUtils, new MockCacheManager());
        ds = new SimpleDriverDataSource(dialect.getDriverInstance(dialect.getDriverName()), "jdbc:postgresql://localhost:5432/pay", "play", "prey");
        jdbcWrapper = Jdbc.openConnection(ds, "contextName", Jdbc.ConnectionMode.CNX_RO, null, executor);
        rDialect = dialect.clone(jdbcWrapper);

        // That will obviously depend on the driver. Feel free to override.
        if (rDialect.hasSchema("space_readonly")) {
            rDialect.dropSchema("space_readonly");
        }

        // Preparation of the schema
        InputStream stream = this.getClass().getResourceAsStream("initial-data-pg.sql");
        String sql = IOUtils.toString(stream, "UTF-16");
        jdbcWrapper.execute(sql);
    }

    /**
     * Dialects with only 'read' capabilities must come with predefined data
     **/
    @Test
    public void testReadCapability() {
        // Actual test
        assertEquals(false, rDialect.hasSchema("doesnt exist"));
        assertEquals(true, rDialect.hasSchema("space_readonly"));

        rDialect.setDefaultSchema("space_readonly");
        assertEquals(true, rDialect.hasTable("products"));
    }

    @Test
    public void testAutocompleteCapability() {
        if (!dialect.isCapabilityAutocomplete()) {
            log.info("Test skipped: The dialect has no autocomplete capability.");
            return;
        }
        List<String> tables = Lists.newArrayList(dialect.listTables("space_readonly", false, null, false).values());
        List<Tuple<String, String>> playsqlTables = rDialect.listPlaySqlTableNames(false);
        assertEquals(false, tables.isEmpty());
        assertEquals(false, playsqlTables.isEmpty());

        rDialect.setDefaultSchema("space_readonly");

        DDLTable tableDef = rDialect.readTableDefinition("products", true);
        assertNotNull(tableDef);

        List<DDLColumn> columns = tableDef.getColumns();
        assertEquals(15, columns.size());
    }

    @Test
    public void testReadWriteCapability() {
        if (!dialect.isCapabilityReadwrite()) {
            log.info("Test skipped: The dialect has no read-write capability.");
            return;
        }

        if (rDialect.hasSchema("space_test")) {
            rDialect.dropSchema("space_test");
        }

        assertEquals(false, rDialect.hasTable("test1"));
        assertEquals(false, rDialect.hasSchema("space_test"));

        rDialect.createSchema("space_test");
        rDialect.setDefaultSchema("space_test");
        rDialect.createSettingsTable();
        rDialect.createAuditTrailTable();
        DDLColumn colID = new DDLColumn("ID", "#", null, null, null, DDLColumn.DataType.INTEGER, null, null, null);
        DDLColumn colPOSITION = new DDLColumn("POSITION", "POSITION", null, null, null, DDLColumn.DataType.TEXT, null, null, null);
        DDLColumn column1 = new DDLColumn("COL1", "Column 1", null, "integer-renderer", null, DDLColumn.DataType.INTEGER, null, null, null);
        DDLColumn column2 = new DDLColumn("COL2", "Column 2", null, "as-string", null, DDLColumn.DataType.TEXT, null, null, null);
        DDLTable table = new DDLTable(true, "test1", "Test 1", colID, colPOSITION, column1, column2);
        rDialect.createTable(table);

        assertEquals(true, rDialect.hasSchema("space_test"));
        assertEquals(true, rDialect.hasTable("test1"));
    }


    @Test
    public void testEntities() {
        if (!dialect.isCapabilityReadwrite()) {
            log.info("Test skipped: The dialect has no read-write capability.");
            return;
        }

        if (rDialect.hasSchema("space_test")) {
            rDialect.dropSchema("space_test");
        }

        assertEquals(false, rDialect.hasTable("test1"));
        assertEquals(false, rDialect.hasSchema("space_test"));

        rDialect.createSchema("space_test");
        rDialect.setDefaultSchema("space_test");
        rDialect.upgradeTask3_createEntitiesTable();

        DDLColumn colID = new DDLColumn("ID", "#", null, null, null, DDLColumn.DataType.INTEGER, null, null, null);
        DDLColumn colPOSITION = new DDLColumn("POSITION", "POSITION", null, null, null, DDLColumn.DataType.TEXT, null, null, null);
        DDLColumn column1 = new DDLColumn("COL1", "Column 1", null, "integer-renderer", null, DDLColumn.DataType.INTEGER, null, null, null);
        DDLColumn column2 = new DDLColumn("COL2", "Column 2", null, "as-string", null, DDLColumn.DataType.TEXT, null, null, null);
        DDLTable table = new DDLTable(true, "test1", "Test 1", colID, colPOSITION, column1, column2);
        rDialect.createTable(table);

        int id = dialect.saveEntity(CapabilityEntities.EntityType.QUERY, null, null, "Label", "Now", "SQL", "{}", true);
        String json = dialect.getEntity(CapabilityEntities.EntityType.QUERY, id).json();
        assertEquals(json, "{}");

        int id2 = dialect.saveEntity(CapabilityEntities.EntityType.QUERY, null, "KEY-1", "Label", "Now", "SQL", "{\"a\":2}", true);
        String json2 = dialect.getEntity(CapabilityEntities.EntityType.QUERY, "KEY-1").json();
        assertEquals(json2, "{\"a\":2}");

        String json3 = dialect.getEntity( null, id).json();
        assertEquals(json3, "{}");

        dialect.deleteEntity(CapabilityEntities.EntityType.QUERY, id);
        assertThat(dialect.getEntity(CapabilityEntities.EntityType.QUERY, id), is(nullValue()));
    }

    @Test
    public void testForeignKeys() {
        if (!dialect.isCapabilityReadwrite()) {
            log.info("Test skipped: The dialect has no read-write capability.");
            return;
        }

        if (rDialect.hasSchema("space_test")) {
            rDialect.dropSchema("space_test");
        }

        assertEquals(false, rDialect.hasTable("test1"));
        assertEquals(false, rDialect.hasSchema("space_test"));

        rDialect.createSchema("space_test");
        rDialect.setDefaultSchema("space_test");
        rDialect.createSettingsTable();
        rDialect.createAuditTrailTable();

        DDLColumn colID = new DDLColumn("ID", "#", null, null, null, DDLColumn.DataType.INTEGER, null, null, null)
            .setOptions(DDLColumn.Option.IDENTITY)
            .setStatusPK(true);
        DDLColumn colPOSITION = new DDLColumn("POSITION", "POSITION", null, null, null, DDLColumn.DataType.TEXT, null, null, null).setStatusPosition(true);
        DDLColumn column1 = new DDLColumn("COL1", "Column 1", null, "integer-renderer", null, DDLColumn.DataType.INTEGER, null, null, null);
        DDLColumn column2 = new DDLColumn("COL2", "Column 2", null, "as-string", null, DDLColumn.DataType.TEXT, null, null, null);
        DDLTable table1 = new DDLTable(true, "test1", "Test 1", colID, colPOSITION, column1, column2);
        DDLColumn fkToTable1 = new DDLColumn("FKToTable1", "Foreign Key", DDLColumn.DataType.INTEGER)
            .setFk(new ForeignKey(null, "test1", "COL1", "COL1", "COL1", ForeignKey.Action.CASCADE, false, false));
        DDLTable table2 = new DDLTable(true, "test2", "Test 2", colID, colPOSITION, column1, column2, fkToTable1);

        rDialect.createTable(table1);
        rDialect.createTable(table2);

        DDLTable tableDefinition = rDialect.readTableDefinition("test2", false);
        DDLColumn fkColumn = tableDefinition.getColumnByName("FKToTable1");
        assertThat(fkColumn, notNullValue());
        assertThat(fkColumn.getFk(), notNullValue());
        assertThat(fkColumn.getFk().getDisplayField(), is("COL1"));
        assertThat(fkColumn.getFk().getTargetTable(), is("test1"));
        assertThat(fkColumn.getFk().getOnDelete(), is(ForeignKey.Action.CASCADE));
        assertThat(fkColumn.getFk().getOnUpdate(), is(ForeignKey.Action.CASCADE));
    }

    @Test
    public void testFormulaDependencies() {
        List<Endpoint> deps;
        if (!dialect.isCapabilityReadwrite()) {
            log.info("Test skipped: The dialect has no read-write capability.");
            return;
        }

        if (rDialect.hasSchema("space_test")) {
            rDialect.dropSchema("space_test");
        }

        assertEquals(false, rDialect.hasTable("test1"));
        assertEquals(false, rDialect.hasSchema("space_test"));

        rDialect.createSchema("space_test");
        rDialect.setDefaultSchema("space_test");
        rDialect.createSettingsTable();
        rDialect.createAuditTrailTable();
        rDialect.upgradeTask2_createFormulaDependenciesTable();

        // columny:6 depends on columnx:3
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", 3L), new Endpoint("tablex", "columny", 6L, "=columnx:3")));
        // columny:7 depends on columnx:*
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", null), new Endpoint("tablex", "columny", 7L, "=SUM(columnx:*)")));
        // columny:8 depends on columny:3
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columny", 3L), new Endpoint("tablex", "columny", 8L, "=columny:3")));
        // columny:9 depends on columnx:4 and columny:3
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columny", 3L), new Endpoint("tablex", "columny", 9L, "=columny:3 + columnx:4")));
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", 4L), new Endpoint("tablex", "columny", 9L, "=columny:3 + columnx:4")));

        // Get all cascades of columnv:1
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columnv", 1L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps.size(), is(0));

        // Get all cascades of columnx:2
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columnx", 2L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, hasItems(new Endpoint("tablex", "columny", 7L)));
        assertThat(deps.size(), is(1));

        // Get all cascades of columnx:3
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columnx", 3L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, hasItems(new Endpoint("tablex", "columny", 6L), new Endpoint("tablex", "columny", 7L)));
        assertThat(deps.size(), is(2));

        // Get all cascades of columnx:4
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columnx", 4L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, hasItems(new Endpoint("tablex", "columny", 7L), new Endpoint("tablex", "columny", 9L)));
        assertThat(deps.size(), is(2));

        // Get all cascades of columny:3
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columny", 3L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, hasItems(new Endpoint("tablex", "columny", 8L), new Endpoint("tablex", "columny", 9L)));
        assertThat(deps.size(), is(2));

        // Get all cascades of *:1
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", null, 1L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, hasItems(new Endpoint("tablex", "columny", 7L)));
        assertThat(deps.size(), is(1));

        // Get all cascades of *:3
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", null, 3L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, hasItems(
                new Endpoint("tablex", "columny", 6L),
                new Endpoint("tablex", "columny", 7L),
                new Endpoint("tablex", "columny", 8L),
                new Endpoint("tablex", "columny", 9L)
        ));
        assertThat(deps.size(), is(4));

        // Get all cascades of *:*
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", null, null), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, hasItems(
                new Endpoint("tablex", "columny", 6L),
                new Endpoint("tablex", "columny", 7L),
                new Endpoint("tablex", "columny", 8L),
                new Endpoint("tablex", "columny", 9L)
        ));
        assertThat(deps.size(), is(4));

        // Get all cascades of nothing
        deps = rDialect.listFormulaDependencies(new Endpoint("tablez", null, null), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps.size(), is(0));

        // Remove the formula in columny:9, and check columny:3 and columnx:4 don't cascade to it anymore
        rDialect.removeFormulaDependencies(new Endpoint("tablex", "columny", 9L), FormulaDependency.EndpointRole.DESTINATION);
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columny", 3L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, not(hasItems(new Endpoint("tablex", "columny", 9L))));
        deps = rDialect.listFormulaDependencies(new Endpoint("tablez", "columnx", 4L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, not(hasItems(new Endpoint("tablex", "columny", 9L))));

        // Remove the formulas for *:8 and check columny:3 doesn't cascade to it anymore
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columny", 3L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, hasItems(new Endpoint("tablex", "columny", 8L)));
        rDialect.removeFormulaDependencies(new Endpoint("tablex", null, 8L), FormulaDependency.EndpointRole.DESTINATION);
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columny", 3L), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps, not(hasItems(new Endpoint("tablex", "columny", 8L))));

        // Remove formulas for columny:* and check there are no cascades anymore
        rDialect.removeFormulaDependencies(new Endpoint("tablex", "columny", null), FormulaDependency.EndpointRole.DESTINATION);
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", null, null), FormulaDependency.EndpointRole.ORIGIN, false, false);
        assertThat(deps.size(), is(0));
    }


    @Test
    public void testFormulaDependencyCascades() {
        List<Endpoint> deps;
        if (!dialect.isCapabilityReadwrite()) {
            log.info("Test skipped: The dialect has no read-write capability.");
            return;
        }

        if (rDialect.hasSchema("space_test")) {
            rDialect.dropSchema("space_test");
        }

        assertEquals(false, rDialect.hasTable("test1"));
        assertEquals(false, rDialect.hasSchema("space_test"));

        rDialect.createSchema("space_test");
        rDialect.setDefaultSchema("space_test");
        rDialect.createSettingsTable();
        rDialect.createAuditTrailTable();
        rDialect.upgradeTask2_createFormulaDependenciesTable();

        // columnx:4 depends on columnx:3
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", 3L), new Endpoint("tablex", "columnx", 4L, "=columnx:3")));
        // columnx:5 depends on columnx:4 (direct dependency)
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", 4L), new Endpoint("tablex", "columnx", 5L, "=columnx:4")));
        // columny:6 depends on columnx:* (direct dependency on a range)
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", null), new Endpoint("tablex", "columny", 6L, "=SUM(columnx:*)")));
        // columny:7 depends on columny:6 and columnx:4 (dependencies must be ordered)
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", 4L), new Endpoint("tablex", "columny", 7L, "=columny:6 + columnx:4")));
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columny", 6L), new Endpoint("tablex", "columny", 7L, "=columny:6 + columnx:4")));
        // columny:8 depends on columny:7 (non-direct dependency on columnx:*)
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columny", 7L), new Endpoint("tablex", "columny", 8L, "=columny:7")));

        // Get all cascades of columny:7: Just columny:8
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columny", 7L), FormulaDependency.EndpointRole.ORIGIN, true, false);
        assertThat(deps, hasItems(new Endpoint("tablex", "columny", 8L)));
        assertThat(deps.size(), is(1));

        // Get all cascades of columnx:1: Must be columny:6 and columny:7 in order
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columnx", 1L), FormulaDependency.EndpointRole.ORIGIN, true, false);
        assertThat(deps.size(), is(3));
        assertThat(deps.get(0), is(new Endpoint("tablex", "columny", 6L)));
        assertThat(deps.get(1), is(new Endpoint("tablex", "columny", 7L)));
        assertThat(deps.get(2), is(new Endpoint("tablex", "columny", 8L)));

        // Get all cascades of columnx:4: Must be columnx:5, columny:6, columny:7 (and columny:8), with 6 before 7
        deps = rDialect.listFormulaDependencies(new Endpoint("tablex", "columnx", 4L), FormulaDependency.EndpointRole.ORIGIN, true, false);
        assertThat(deps.size(), is(4));
        int pos5 = deps.indexOf(new Endpoint("tablex", "columnx", 5L));
        int pos6 = deps.indexOf(new Endpoint("tablex", "columny", 6L));
        int pos7 = deps.indexOf(new Endpoint("tablex", "columny", 7L));
        assertTrue(pos5 != -1);
        assertTrue(pos6 != -1);
        assertTrue(pos7 != -1);
        assertTrue(pos6 < pos7);
    }

    @Test(expected = CircularFormulaException.class)
    public void testFormulaDependencyInfiniteRecursion() {
        if (!dialect.isCapabilityReadwrite()) {
            log.info("Test skipped: The dialect has no read-write capability.");
            return;
        }

        if (rDialect.hasSchema("space_test")) {
            rDialect.dropSchema("space_test");
        }

        assertEquals(false, rDialect.hasTable("test1"));
        assertEquals(false, rDialect.hasSchema("space_test"));

        rDialect.createSchema("space_test");
        rDialect.setDefaultSchema("space_test");
        rDialect.createSettingsTable();
        rDialect.createAuditTrailTable();
        rDialect.upgradeTask2_createFormulaDependenciesTable();

        // columnx:4 depends on columnx:3
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", 3L), new Endpoint("tablex", "columnx", 4L, "=columnx:3")));
        // columnx:5 depends on columnx:4 (direct dependency)
        rDialect.saveFormulaDependency(new FormulaDependency(new Endpoint("tablex", "columnx", 4L), new Endpoint("tablex", "columnx", 3L, "=columnx:4")));

        // This should throw an infinite formula recursion error
        rDialect.listFormulaDependencies(new Endpoint("tablex", "columnx", 3L), FormulaDependency.EndpointRole.ORIGIN, true, false);
        // This code should never be reached
        assertTrue(false);
    }

    @After
    public void shutdown() {
        if (jdbcWrapper != null) {
            jdbcWrapper.commit();
            jdbcWrapper.closeSilently();
        }
    }
}
