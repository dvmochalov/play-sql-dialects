package com.playsql.spi.models;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/** Too bad I have no need for a pentuple, hexuple, heptuple, octuple.
 * Well, still allows for safe type-checking while not requiring a new bean each time **/
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Tuple<First, Second> implements Serializable
{
    protected First first;
    protected Second second;

    public Tuple(First first, Second second) {
        if (first != null && !(first instanceof Serializable) && !(first instanceof List)) {
            throw new IllegalArgumentException("Parameters must be serializable: " + first.getClass().getCanonicalName());
        }
        if (second != null && !(second instanceof Serializable) && !(second instanceof List)) {
            throw new IllegalArgumentException("Parameters must be serializable: " + second.getClass().getCanonicalName());
        }
        this.first = first;
        this.second = second;
    }

    public First first()
    {
        return first;
    }

    public Second second()
    {
        return second;
    }

    public First getFirst() { return first(); }
    public Second getSecond() { return second(); }

    public static <First, Second> Map<First, Second> asMap(List<Tuple<First, Second>> list)
    {
        Map<First, Second> map = Maps.newHashMap();
        for (Tuple<First, Second> tuple : list)
            map.put(tuple.first(), tuple.second());
        return map;
    }

    public static <First, Second> Tuple<First, Second> of(First first, Second second) {
        return new Tuple<First, Second>(first, second);
    }

    public String toString() {
        return getClass().getSimpleName() + "{" + Objects.toString(first) + ", " + Objects.toString(second) + "}";
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Truple<First, Second, Third> extends Tuple<First, Second>{

        protected final Third third;

        public Truple(First first, Second second, Third third) {
            super(first, second);
            if (third != null && !(third instanceof Serializable) && !(third instanceof List)) {
                throw new IllegalArgumentException("Parameters must be serializable: " + third.getClass().getCanonicalName());
            }
            this.third = third;
        }

        public static <First, Second, Third> Truple<First, Second, Third> of(First first, Second second, Third third) {
            return new Truple<First, Second, Third>(first, second, third);
        }

        public Third third() {
            return third;
        }
        public Third getThird() { return third(); }

        public static <First, Second, Third> List<Object[]> asArray(List<Truple<First, Second, Third>> list) {
            return Lists.transform(list, input -> new Object[]{input.first(), input.second(), input.third()});
        }

        public String toString() {
            return getClass().getSimpleName() + "{" + Objects.toString(first) + ", " + Objects.toString(second) + ", " + Objects.toString(third) + "}";
        }
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Tetruple<First, Second, Third, Fourth> extends Tuple.Truple<First, Second, Third> {

        private final Fourth fourth;

        private Tetruple(First first, Second second, Third third, Fourth fourth) {
            super(first, second, third);
            if (fourth != null && !(fourth instanceof Serializable) && !(fourth instanceof List)) {
                throw new IllegalArgumentException("Parameters must be serializable: " + fourth.getClass().getCanonicalName());
            }
            this.fourth = fourth;
        }

        public static <First, Second, Third, Fourth> Tetruple<First, Second, Third, Fourth> of(First first, Second second, Third third, Fourth fourth) {
            return new Tetruple<>(first, second, third, fourth);
        }

        public Fourth fourth() { return fourth; }



        public String toString() {
            return getClass().getSimpleName() + "{" + Objects.toString(first) + ", " + Objects.toString(second) + ", " + Objects.toString(third) + ", " + Objects.toString(fourth) + "}";
        }
    }

}
