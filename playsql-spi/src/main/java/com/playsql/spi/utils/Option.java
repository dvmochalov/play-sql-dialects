package com.playsql.spi.utils;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;

public class Option implements Serializable {
    private Object value;
    public Option(Object value) {
        this.value = value;
    }
    public Object get() { return value; };
    public <T> T get2() { return (T) value; };
    public static Option none() { return new Option(null); }
    public static Option some(Object value) { return new Option(value); }
    public <T> T getOrElse(T defaultValue) { return value != null ? (T) value : defaultValue; }
}
